const Mask = {
    Phone: {
        ru: String.raw`\+7 \([1-9]\d\d\) \d\d\d\-\d\d\-\d\d`,
        en: String.raw`\+1 \([1-9]\d\d\) \d\d\d\-\d\d\d\d`
    },
    Email: String.raw`[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,4}`,
    Time: {
        FromTo: String.raw`((?:[01]\d|2[0-3]):[0-5]\d-(?:[01]\d|2[0-3]):[0-5]\d)`
    }
};
export default Mask;