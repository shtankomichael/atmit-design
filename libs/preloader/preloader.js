﻿import './preloader.less';
import I18n from './i18n/i18n'

export default class Preloader {
    static get defaultOptions() {
        return {
            id: undefined,
            node: undefined,
            lang: 'ru',
            text: null,
            isFixed: false,
            isBlockEnabled: true,
            idBlockElement: undefined
        };
    }
    static show(options = { isFixed: true }) {
        Object.merge(options, Preloader.defaultOptions);
        if (options.text == null)
            options.text = I18n[options.lang]['loading'];

        const wrapper = document.createElement('div');
        wrapper.classList.add('loader-wrapper');
        if (options.isFixed)
            wrapper.classList.add('fixed');
        wrapper.prepend(Preloader._getLoaderHtml(options.text));
        wrapper.hide = () => { return false; };

        let node;
        if (options.id == null && options.node == null)
            node = document.body;
        else if (options.node != null)
            node = options.node;
        else
            node = document.getElementById(options.id);

        if (node == null)
            return wrapper;
        node.classList.add('position-relative');
        node.prepend(wrapper);

        const loaderBlock = document.createElement('div');
        if (options.isBlockEnabled) {
            loaderBlock.classList.add('loader-block');
            if (options.idBlockElement == null)
                node.append(loaderBlock);
            else
                document.getElementById(options.idBlockElement).append(loaderBlock);
        }

        wrapper.hide = () => {
            node.classList.remove('position-relative');
            wrapper.remove();
            loaderBlock.remove();
        };
        return wrapper;
    }

    static _getLoaderHtml(text) {
        const loader = document.createElement('div');
        loader.classList.add('loader');
        let loaderHtml = '<div class="circle">' +
            '<div class="circle inner"></div>' +
            '</div>';
        if (text != null)
            loaderHtml += `<span class="loader-text">${text}<span class='loading-dots'></span></span>`;
        loader.innerHTML += loaderHtml;
        return loader;
    }
}