﻿export default  {
    'areYouSureYouWantCloseWindow': 'Вы уверены, что хотите закрыть это окно?',
    'yes': 'Да',
    'no': 'Нет',
    'close': 'Закрыть'
};