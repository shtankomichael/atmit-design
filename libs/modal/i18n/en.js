﻿export default  {
    'areYouSureYouWantCloseWindow': 'Are you sure you want to close window?',
    'yes': 'Yes',
    'no': 'No',
    'close': 'Close'
};