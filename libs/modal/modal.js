﻿import './modal.less';
import i18n from './i18n/i18n';
import Logger from '../../helpers/logger/logger';
import Request from '../../helpers/request/request';

const ModalType = {
    Alert: 'Alert',
    Confirm: 'Confirm',
    Custom: 'Custom'
};

class Modal {

    constructor(opts = {}) {
        let lang = opts.lang ? opts.lang : 'ru';
        this.i18n = i18n[lang];

        Object.merge(opts, this.defaultOptions);
        this.options = opts;        

        this._init().then(() => {
            this.options.onLoaded();
        });
        this.show();
    }    

    get defaultOptions() {
        const id = new Date().getTime();
        return {
            id: `modal-${id}`,
            lang: 'ru',
            title: '',
            class: 'modal',
            //useOverlay: true,
            styles: {
                width: '600px'
            },
            escToClose: true,
            clickOverlay: true,
            customEvents: { },
            onClosed: () => { }, //модал закрыт
            onShown: () => { }, //модал открыт
            onLoaded: () => { }, //все данные загружены
            closeConfirm: {
                enable: false,
                conditions: () => true,
                text: this.i18n['areYouSureYouWantCloseWindow']
            },
            content: {
                html: null,
                ractiveOptions: {
                    component: null,
                    parentRactive: null,
                    ractive: null,
                    wrapper: `wrapper-${id}`
                },
                request: {
                    method: 'GET',
                    async: true,
                    url: null,
                    data: {}
                }
            },
            type: ModalType.Alert,
            buttons: {
                Confirm: {
                    accept: {
                        class: 'btn-confirm',
                        text: this.i18n['yes'],
                        attrs: {},
                        callback: () => { }
                    },
                    cancel: {
                        class: 'btn-cancel',
                        text: this.i18n['no'],
                        attrs: {},
                        callback: () => {
                            this.close();
                        }
                    }
                },
                Alert: {
                    close: {
                        class: null,
                        text: this.i18n["close"],
                        attrs: {},
                        callback: () => {
                            this.close();
                        }
                    }
                },
                Custom: {
                }
            }
        }
    }

    _init() {
        return new Promise((resolve, reject) => {

            this.htmlElement = document.createElement('div');
            this.htmlElement.setAttribute('id', this.options.id);
            this.htmlElement.classList.add(this.options.class);

            const headerEl = document.createElement('div');
            headerEl.classList.add('header');

            const titleEl = document.createElement('div');
            titleEl.classList.add('title');
            titleEl.innerHTML = this.options.title;

            const closeEl = document.createElement('div');
            closeEl.classList.add('close-modal');
            closeEl.innerHTML = '<span>&#43;</span>';
            closeEl.addEventListener('click', () => {
                this.close();
            });

            headerEl.appendChild(titleEl);
            headerEl.appendChild(closeEl);

            this.htmlElement.appendChild(headerEl);

            const content = document.createElement('div');
            content.classList.add('content');

            if (this.options.content.html != null) {
                this._insertHtml(content, this.options.content.html);
            } else if (this.options.content.ractiveOptions.component) {
                this._createRactive(content);
            }
            this.htmlElement.appendChild(content);
            if (this.options.buttons[this.options.type] == null)
                console.error('unknown button type');
            const buttons = Object.values(this.options.buttons[this.options.type]);
            if (buttons.length > 0) {
                const buttonsEl = document.createElement('div');
                buttonsEl.classList.add('buttons');

                for (let btn of buttons) {
                    let btnEl = document.createElement('button');
                    btnEl.innerHTML = btn.text;

                    if (btn.class != null)
                        btnEl.classList.add(btn.class);
                    if (btn.attrs != null) {
                        for (let attr in btn.attrs) {
                            btnEl.setAttribute(attr, btn.attrs[attr]);
                        }
                    }
                    if (typeof btn.callback === 'function')
                        btnEl.addEventListener('click', () => {
                            btn.callback();
                        });

                    buttonsEl.appendChild(btnEl);
                }
                this.htmlElement.appendChild(buttonsEl);
            }

            this._initStyles();

            const modalOverlay = document.createElement('div');
            modalOverlay.classList.add('modal-overlay');
            modalOverlay.appendChild(this.htmlElement);

            if (this.options.clickOverlay === true)
                modalOverlay.addEventListener('click',
                    (e) => {
                        if (e.target !== modalOverlay)
                            return false;
                        this.close();
                    });


            this.body = modalOverlay;
            for (let key in this.options.customEvents)
                this[key] = this.options.customEvents[key];

            Modal.modals[this.options.id] = this;

            if (this.options.content.request.url == null)
                resolve();
        });
    }

    _initStyles() {
        for (let propKey in this.options.styles)
            this.htmlElement.style[propKey] = this.options.styles[propKey];
    }

    _createRactive(el) {

        const ractiveWrapper = document.createElement('div');
        ractiveWrapper.setAttribute('id', this.options.content.ractiveOptions.wrapper);
        el.appendChild(ractiveWrapper);

        this.options.content.ractiveOptions.ractive =
            new this.options.content.ractiveOptions.component({
                el: ractiveWrapper
            });

        this.options.content.ractiveOptions.ractive.on('_close', () => {
            this.close();
        });
        this.options.content.ractiveOptions.ractive['_close'] = () => {
            this.close();
        };

        this.options.content.ractiveOptions.ractive.set('_page.idModal', this.options.id);

        if (this.parentRactive != null)
            this.parentRactive.attachChild(this.ractive);
    }

    _loadScript(el, script) {
        return new Promise((resolve, reject) => {
            let src = script.getAttribute('src');
            let type = script.getAttribute('type');
            let id = script.getAttribute('id');

            if (type == null)
                type = 'text/javascript';

            let newScript = document.createElement('script');
            if (src != null && src !== '')
                newScript.src = src;

            if (id != null && id !== '')
                newScript.id = id;

            newScript.type = type;
            newScript.innerHTML = script.innerHTML;
            newScript.onload = resolve;
            newScript.onerror = reject;

            script.parentNode.insertBefore(newScript, script);
            script.remove();
        });
    }

    _insertHtml(el, html) {
        el.innerHTML += html;
        let scripts = el.getElementsByTagName("script");
        let promises = [];

        for (let i = 0; i < scripts.length; i++)
            promises.push(this._loadScript(el, scripts[i]));

        Promise.all(promises);
    }

    _showBodyScrollbar() {
        document.body.style.paddingRight = 0;
        document.body.style.overflowY = 'auto';
    }

    _hideBodyScrollbar() {
        document.body.style.paddingRight = window.innerWidth - document.body.offsetWidth + 'px';
        document.body.style.overflowY = 'hidden';
    }

    destroy() {

        if (this.parentRactive != null)
            this.parentRactive.detachChild(this.ractive);
        if (this.ractive != null) {
            this.ractive.teardown();
        }

        this._showBodyScrollbar();
        this.body.remove();
        this.options.onClosed();

        delete Modal.modals[this.options.id];
    }

    show() {

        this._hideBodyScrollbar();
        document.body.appendChild(this.body);
        this.options.onShown();
    }

    close() {
        if (this.options.closeConfirm.enable === true && this.options.closeConfirm.conditions() === true) {

            const _this = this;
            this.body.style.overflowY = 'hidden';

            const confirmModal = new Modal({
                title: _this.options.closeConfirm.text,
                type: ModalType.Confirm,
                onClosed: () => {
                    _this.body.style.overflowY = 'auto';
                },
                buttons: {
                    Confirm: {
                        accept: {
                            callback: () => {
                                confirmModal.close();
                                _this.destroy();
                            }
                        },
                        cancel: {
                            callback: () => {
                                confirmModal.close();
                                _this.body.style.overflowY = 'auto';
                            }
                        }
                    }
                }
            });
        } else {
            this.destroy();
        }
    }

    static get(id) {
        return Modal.modals[id];
    }

    get ractive() {
        return this.options.content.ractiveOptions.ractive;
    }

    get parentRactive() {
        return this.options.content.ractiveOptions.parentRactive;
    }

    static close(id) {
        Modal.get(id).close();
    }
}

Modal.modals = {};
Modal.Type = ModalType;
window.addEventListener('keydown', function (e) {
    if (e.key == 'Escape' || e.key == 'Esc' || e.keyCode == 27) {
        const modalKeys = Object.keys(Modal.modals);
        if (modalKeys.length > 0) {
            const lastModal = Modal.get(modalKeys[modalKeys.length - 1]);
            if (lastModal.options.escToClose === true)
                lastModal.close();
        }
        e.preventDefault();
        return false;
    }
}, true);

export default Modal;