﻿import './multi-dropdown-list.scss';
import I18n from './i18n/i18n';
import Request from '../../helpers/request/request';

export default class MultiDDL {
    constructor(options) {
        let lang = options.lang ? options.lang : 'ru';
        this.i18n = I18n[lang];

        let opts = this.defaultOptions;
        $.extend(true, opts, options);
        this.selectedCount = 0;
        this.isInited = false;
        this.state = 0; //0-closed, 1-open
        $.each(opts, (param, value) => {
            this[param] = value;
        });
        this.init();
    }

    get defaultOptions() {
        return {
            element: null,
            width: '100%',
            height: 'auto',
            heightTitle: 'auto',
            isReadonly: false,
            lang: 'ru',
            argName: null, // имя параметра в форме (атрибут name)
            titleInput: {
                useAsOutput: true, //использовать строку для вывода значений, служит для окрытия/закрытия списка 
                useTags: true, //использовать теги вместо строки с запятыми
                addTags: false, //добавлять пользовательские теги
                tagItem: (item, label) => {
                    var uniqField = item[this.uniqueFieldName];
                    var span = $('<span data-id="' + uniqField + '" class="multiDDL_label">' + label + '</span>');
                    var removebBtn = $('<button class="remove_label_btn">&times;</button>').appendTo(span);
                    removebBtn.on('click', (e) => {
                        e.preventDefault();
                        e.stopPropagation();
                        var checkboxId = this.genCheckboxName(uniqField);
                        var checkbox = this.ui.itemList.find("[data-id-checkbox='" + checkboxId + "']");
                        if (checkbox.length > 0) {
                            checkbox.prop('checked', false).trigger('change');
                        } else {
                            var index = 0;
                            for (var i = 0; i < this.selectedItems.length; i++) {
                                if (this.selectedItems[i][this.uniqueFieldName] == uniqField) {
                                    index = i;
                                    break;
                                }
                            }
                            if (index > -1) {
                                this.selectedItems.splice(index, 1);
                                this.selectedCount -= 1;
                            }
                            this.updateTitle(item, false);
                            if (this.ui.inputElem != null) {
                                this.ui.inputElem.find("option[value='" + uniqField + "']").remove();
                            }
                            if (typeof (this.checkboxes.onChange) == "function") {
                                this.checkboxes.onChange(false, item);
                            }
                        }
                    });
                    this.ui.title.append(span);
                },
                updateTagItem: (item) => {
                    this.ui.title.find('span[data-id="' + item[this.uniqueFieldName] + '"]').remove();
                },
                onCreateNewItem: (text) => { //как создать элемент из тега
                    var item = {};
                    item[this.uniqueFieldName] = text;
                    return item;
                }
            },
            defaultValue: "<span class='default_selected_val'>" + this.i18n['nothingSelected'] + "</span>", //placeholder, если ничего не выбрано
            floatingDropdown: true, //плавающее поверх всего выпадающее меню
            items: [], //элементы по умолчанию
            actions: [], //массив кнопок каждого элемента
            uniqueFieldName: 'id', // имя поля, являющ. уникальным (по которому можно найти элемент в списке, напр., Id)
            url: {
                link: null, //url для получения элементов
                method: "POST", //тип метод
                data: {}, //данные для передачи
                processResults: (data) => {
                    return data;
                } //метод, который преобразует данные к нужному формату
            },
            search: {
                enable: false, //активен ли поиск
                serverSearch: false, // поиск будет производится на сервере
                serverSearchArgName: 'search', // название аргумента поиска на сервере (используется когда поиск реализован в той же функции, что и получение элементов url.link)
                string: null, //дефолтный поиск
                placeholder: this.i18n['search'],
                onSearch: (item, searchStr) => {
                    return item[this.uniqueFieldName].includes(searchStr);
                }, //удовлетворяет ли поиску элемент
                onServerSearch: null, // поиск на сервере, в onDone передаются элементы function (searchStr, onDone) { }
                innerTitleInput: false // где будет распологаться поиск (true - внутри строки с тегами, false - вне строки тегов)
            },
            monoMode: false, //режим работы выпадающего списка (мульти, моно)
            checkboxes: { //правила для чекбоксов   
                isChecked: (item) => {
                    if (this.uniqueFieldName != null) {
                        for (var i = 0; i < this.selectedItems.length; i++) {
                            if (this.selectedItems[i][this.uniqueFieldName] == item[this.uniqueFieldName])
                                return true;
                        }
                    }
                    return this.selectedItems.indexOf(item) != -1;
                }, //делегат для определения выбран ли элемент списка или нет (использует внутри uniqueFieldName)!!!!!!!!!!!!!!!!!!!!!!
                onChange: (isChecked, item, el) => {} //событие при изменении чекбокса
            },
            selectedItems: [], //выбранные элементы (1 если monoMode)
            onSelected: null, //произошел выбор элемента (только если monoMode)
            createItem: (item) => {
                var span = $('<span>').addClass('pointer').html(this.getItemTitle(item));
                if (!this.monoMode)
                    span.on('click', function (e) {
                        this.parentNode.parentNode.children[1].click();
                    });
                return span;
            }, //делегат для создания элемента списка
            getItemTitle: (item) => {
                return item.text;
            }, //как из элемента получить заголовок
            createBottom: null, //делегат как создать нижнюю часть
            createPlaceholderIfEmpty: () => {
                return "<span>" + this.i18n['nothingFound'] + "</span>";
            }, //placeholder, когда нет элементов
            nonClosableClass: null, //список не закрывается, если клик произошел вне области списка, но на элементы с данными классом
            onClose: () => {}, //при закрытии списка
            onInited: () => {} // инициализация
        };
    }
    init() {
        this.mainClass = "dropdown_multiDDL";
        if (this.element == undefined || this.element.length === 0 || $(this.element).length === 0) {
            console.error("multiDDL: Element with id=" + this.element + " was not found");
        }
        this.ui = $(this.element);

        this.createDDL();
        if (!this.isReadonly) {
            if (this.search.enable)
                this.createSearch();
            this.beforeCreateContent("init");
        }
    }

    /**
     * создает макет DDL
     */
    createDDL() {

        if (this.argName != null) {
            if (this.uniqueFieldName == null) {
                console.error("multiDDL: uniqueFieldName is null");
            } else {
                this.ui.inputElem = this.monoMode == true ? $("<input>") : $("<select multiple>");
                this.ui.inputElem.attr("name", this.argName).css("display", "none");
                this.ui.inputElem.appendTo(this.ui);
            }
        }

        //главный контейнер
        this.ui.dl = $("<dl>")
            .appendTo(this.ui)
            .css("width", this.width)
            .addClass(this.mainClass);
        //контейнер заголовка
        this.ui.dt = $("<dt>")
            .appendTo(this.ui.dl);
        //заголовок
        this.ui.dt.a = $("<a>").addClass("multiSel");
        try {
            var heightTitle = parseInt(this.heightTitle);
            this.ui.dt.a.css("height", heightTitle + "px");
            this.ui.dt.a.css("line-height", (heightTitle - 1) + "px");
        } catch (e) {}

        if (this.titleInput.useAsOutput) {
            this.ui.dt.a.appendTo(this.ui.dt);
        }

        this.ui.dt.a.title = $("<div>").addClass("title")
            .appendTo(this.ui.dt.a);
        this.ui.dt.a.title.inner = $("<div>").addClass("title-inner")
            .appendTo(this.ui.dt.a.title);

        this.ui.dt.a.arrowBlock = $("<div>").addClass("arrow")
            .appendTo(this.ui.dt.a);
        var arrowMargin = this.ui.dt.height() / 2 - 7;
        this.ui.dt.a.arrow = $("<div>").addClass("arrow-down")
            .appendTo(this.ui.dt.a.arrowBlock);

        this.ui.title = this.ui.dt.a.title.inner;
        this.ui.titleArrow = this.ui.dt.a.arrow;

        if (this.isReadonly)
            this.ui.dt.a.addClass("readonly");

        if (this.selectedItems.length != 0) {
            for (var i = 0; i < this.selectedItems.length; i++) {
                this.updateTitle(this.selectedItems[i], true);
            }
        } else if (this.defaultValue != null) {
            this.ui.title.html(this.defaultValue);
        }

        //контейнер меню
        this.ui.dd = $("<dd>")
            .appendTo(this.ui.dl);

        //контейнер списка элементов
        this.ui.dd.multiSelect = $("<div>")
            .appendTo(this.ui.dd)
            .addClass("mutliSelect");
        //список элементов
        this.ui.dd.multiSelect.ul = $("<ul>").addClass('result-list').appendTo(this.ui.dd.multiSelect)
            .css({
                "max-height": this.height
            });
        if (this.floatingDropdown == true) {
            this.ui.dd.multiSelect.ul.css("position", "absolute");
        }
        this.ui.itemList = this.ui.dd.multiSelect.ul;

        //события
        if (this.titleInput.useAsOutput) {
            this.ui.dt.a.on('click', (e) => {
                if (this.state == 1 && this.search.innerTitleInput) {
                    var target = $(e.target);
                    if (target.hasClass('search-input'))
                        return;
                }
                this.toggleDropdown();
            });
        }

        $(document).bind('click', (e) => {
            var target = $(e.target);
            if (!target.hasClass(this.mainClass) && !target.parents().hasClass(this.mainClass))
                if (!target.hasClass(this.nonClosableClass) && !target.parents().hasClass(this.nonClosableClass)) {
                    this.closeDropdown();
                    if (typeof (this.onClose) == "function") {
                        this.onClose();
                    }
                }
        });
    }

    disable() {
        this.ui.dt.a.addClass("readonly");
    }
    enable() {
        this.ui.dt.a.removeClass("readonly");
    }

    //поисковая строка
    createSearch() {

        var onSearch = () => {
            var searchStr = this.ui.dd.search.string.val();
            if (searchStr != null && searchStr != "")
                this.search.string = searchStr;
            else this.search.string = null;
            this.beforeCreateContent("search");
        };

        if (this.search.innerTitleInput && this.monoMode == false) {
            this.ui.dd.search = $("<span>")
                .appendTo(this.ui.dt.a.title)
                .addClass('search-block');

            this.ui.dd.search.string = $("<input type='text'>")
                .appendTo(this.ui.dd.search)
                .val(this.search.string)
                .attr('placeholder', this.search.placeholder)
                .addClass('search-input form-control form-control-sm')
                .on('keyup', (e) => {
                    onSearch();
                })
                .on('keydown', (e) => {

                    if (e.keyCode == 13) {
                        e.stopPropagation();
                        e.preventDefault();

                        if (this.titleInput.addTags) {
                            var searchStr = this.ui.dd.search.string.val();
                            if (searchStr != null && searchStr != "")
                                this.search.string = searchStr;
                            else this.search.string = null;

                            //тут проверка что такого элемента нет
                            var newItem = this.titleInput.onCreateNewItem(this.search.string);
                            var index = -1;
                            for (var i = 0; i < this.items.length; i++) {
                                if (this.items[i][this.uniqueFieldName] == newItem[this.uniqueFieldName]) {
                                    newItem = this.items[i];
                                    index = i;
                                    break;
                                }
                            }
                            if (index == -1) {
                                this.items.push(newItem);
                            }
                            this.setSelection(newItem);
                            this.ui.dd.search.string.val(null);
                            onSearch();
                        }
                        return false;
                    }
                });
        } else {
            this.ui.dd.search = $("<div>")
                .prependTo(this.ui.dd.multiSelect)
                .addClass('search-block');
            if (this.floatingDropdown) {
                this.ui.dd.search.css({
                    position: 'absolute'
                });
            }
            this.ui.dd.search.string = $("<input type='text'>")
                .appendTo(this.ui.dd.search)
                .val(this.search.string)
                .attr('placeholder', this.search.placeholder)
                .addClass('search-input form-control form-control-sm')
                .on('keyup', (e) => {
                    onSearch();
                });
            this.ui.itemList.css('top', '26px');
        }
    }

    // внутренний контент DDL
    beforeCreateContent(action) {
        switch (action) {
            case "init":
                {
                    // по умолчанию есть элементы
                    if (this.items.length > 0 || this.url.link == null) {
                        this.createItems();
                    } else {
                        this.downloadItems(() => {
                            this.createItems();
                        });
                    }
                    break;
                }
            case "search":
                {
                    if (this.search.serverSearch == true) {
                        if (typeof (this.search.onServerSearch) == "function") {
                            this.search.onServerSearch(this.search.string,
                                (items) => {
                                    this.items = items;
                                    this.createItems();
                                });
                        } else {
                            this.downloadItems(() => {
                                this.createItems();
                            });
                        }
                    } else {
                        this.createItems();
                    }
                    break;
                }
        }
    }

    clearTitle() {
        this.ui.title.find('span:not(.search-block)').html("");
    }

    //обновляет заголовок (список выбранных элементов)
    updateTitle(item, isChecked) {
        if (!this.titleInput.useAsOutput || typeof (this.getItemTitle) != "function")
            return;
        var label = this.getItemTitle(item);

        var uniq = item[this.uniqueFieldName];
        if (this.monoMode) {
            this.ui.title.html('<span data-id="' + uniq + '">' + label + '</span>');
        } else {
            if (this.selectedCount <= 0 && !isChecked) {
                if (this.defaultValue != null) {
                    this.ui.title.html(this.defaultValue);
                    return;
                }
            } else if (this.selectedCount == 1 && isChecked) {
                this.clearTitle();
            }

            if (isChecked) {
                var span;
                if (this.titleInput.useTags) {
                    this.titleInput.tagItem(item, label);
                } else {
                    span = '<span data-id="' + uniq + '">' + label + ", " + '</span>';
                    this.ui.title.append(span);
                }
            } else {
                this.titleInput.updateTagItem(item);
            }
        }
    }
    getGuid() {
        let s4 = () => Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    //генерация каждого элемента
    createItems() {
        var list = $("<div>");
        if (this.items == undefined) console.error('multiDDL: this.items - undefined (empty data array)');
        var hasItems = false;
        this.items.forEach((item, i) => {
            var uniq = item[this.uniqueFieldName];
            if (this.search.serverSearch == false && this.search.string != null) {
                if (!this.search.onSearch(item, this.search.string))
                    return;
            }

            hasItems = true;
            var li = $("<li>").data("index", i);
            var row = $("<div>")
                .appendTo(li).addClass("row");
            var checkboxDiv = $("<div>")
                .appendTo(row)
                .addClass("col-" + (this.actions.length == 0 ? 12 : (12 - (this.actions.length + 1))));
            //если !monoMode, то выбор происходит при клике на чекбоксы

            if (!this.monoMode) {
                var uniqId = this.getGuid();
                checkboxDiv.addClass("checkbox");
                var checkboxId = this.genCheckboxName(uniq);
                var checkbox = $("<input class='mt-checkbox' type='checkbox' data-id-checkbox='" + checkboxId + "' id='" + uniqId + "'>")
                    .appendTo(checkboxDiv);
                var checkboxLbl = $("<label for='" + uniqId + "'></label>").appendTo(checkboxDiv);

                if (typeof (this.checkboxes.isChecked) == "function") {
                    var isChecked = this.checkboxes.isChecked(item);
                    checkbox.prop("checked", isChecked);

                    if (isChecked == true && this.isInited == false) {
                        //выбора элемента при инициализации
                        this.setSelectedMulti(item, isChecked);
                    }
                }

                checkbox.on("change", () => {
                    this.setSelectedMulti(item, checkbox.is(":checked"));
                });
            }

            var itemContent = $("<div>")
                .appendTo(checkboxDiv)
                .addClass("item-inner-content");

            var userContent;
            if (typeof (this.createItem) == "function") {
                userContent = this.createItem(item, row);
                itemContent.html(userContent);
            }

            //если monoMode, то выбор происходит при клике на любой элемент
            if (this.monoMode) {
                var selectFunc = () => {
                    this.setSelectedMono(item);
                };
                var selectable = itemContent.find("." + this.mainClass + "_selectable");
                if (selectable.length == 0) {
                    itemContent.on("click", selectFunc);
                } else {
                    selectable.on("click", selectFunc);
                }
            }

            if (this.actions.length > 0) {
                var actionsDiv = $("<div>")
                    .appendTo(row)
                    .addClass("col-" + (this.actions.length + 1));
                var groupBtn = $("<div>")
                    .appendTo(actionsDiv)
                    .addClass("btn-group")
                    .attr("role", "group");
                this.actions.forEach((action, i) => {
                    var actionBtn = $("<button>")
                        .appendTo(groupBtn)
                        .addClass("btn")
                        .addClass(action.btnClass)
                        .attr("title", action.title);
                    if (typeof (action.onClick) == "function") {
                        actionBtn.on("click", (e) => {
                            e.preventDefault();
                            action.onClick(item, userContent, row);
                        });
                    }
                    var icon = $("<icon>")
                        .appendTo(actionBtn)
                        .addClass(action.icon);
                });
            }
            li.appendTo(list);
        });

        if (!hasItems) {
            //placeholder
            this.createPlaceholder(list);
        }

        if (typeof (this.createBottom) == "function") {
            var bottomContent = this.createBottom();
            var bottom = $("<li class='last'>");
            bottom.html(bottomContent);
            bottom.appendTo(list);
        }

        this.ui.itemList.html(list);
        if (this.isInited == false) {
            this.selectedCount = this.selectedItems.length;
            this.isInited = true;
            this.onInited();
        }
    }

    genCheckboxName(uniq) {
        if (Number.isFinite(uniq))
            return uniq;
        return uniq.replace(' ', "_");
    }

    setSelectedMulti(item, isChecked, disableTriggerEvent) {
        var uniq = item[this.uniqueFieldName];
        if (isChecked) {
            this.selectedItems.push(item);
            this.selectedCount += 1;
            this.updateTitle(item, isChecked);
            if (this.ui.inputElem != null) {
                var option = $("<option value='" + uniq + "' selected>").appendTo(this.ui.inputElem);
            }
        } else {
            var index = -1;
            for (var i = 0; i < this.selectedItems.length; i++) {
                if (this.selectedItems[i][this.uniqueFieldName] == uniq) {
                    index = i;
                    break;
                }
            }

            if (index > -1) {
                this.selectedItems.splice(index, 1);
                this.selectedCount -= 1;
            }
            this.updateTitle(item, isChecked);
            if (this.ui.inputElem != null) {
                this.ui.inputElem.find("option[value='" + uniq + "']").remove();
            }
        }

        var checkboxId = this.genCheckboxName(uniq);
        var checkbox = this.ui.itemList.find("[data-id-checkbox='" + checkboxId + "']");

        if (checkbox.length > 0) {
            checkbox.prop('checked', isChecked);
        }

        if (typeof (this.checkboxes.onChange) == "function" && disableTriggerEvent !== true) {
            this.checkboxes.onChange(isChecked, item);
        }
    }

    setSelectedMono(item, disableTriggerEvent) {
        this.closeDropdown();
        this.updateTitle(item);
        this.selectedItems = [];
        this.selectedItems.push(item);
        if (this.ui.inputElem != null) {
            var uniq = item[this.uniqueFieldName];
            this.ui.inputElem.val(uniq);
        }
        if (typeof (this.onSelected) == "function" && disableTriggerEvent !== true) {
            this.onSelected(item);
        }
    }

    createPlaceholder(list) {
        if (typeof (this.createPlaceholderIfEmpty) == "function") {
            var row = $("<div>").addClass("row-fluid");
            var checkboxDiv = $("<div>")
                .appendTo(row)
                .addClass("span12");
            var itemContent = $("<div>")
                .appendTo(checkboxDiv)
                .addClass("item-inner-content")
                .css({
                    'text-align': 'center',
                    'padding': '5px'
                });
            var content = this.createPlaceholderIfEmpty();
            itemContent.html(content);
            row.appendTo(list);
        }
    }

    downloadItems(_callback) {
        var data = this.url.data;
        if (this.search.serverSearch == true && this.search.serverSearchArgName != null) {
            data[this.search.serverSearchArgName] = this.search.string;
        }

        Request.ajax({
            url: this.url.link,
            method: this.url.method,
            data: data
        }).then((res) => {
            if (res == null || res.success == false) {
                console.error('multiDDL');
                return;
            }
            this.items = this.url.processResults(res);
            _callback();
        });
    }

    toggleDropdown() {
        if (this.isReadonly)
            return;
        if (this.state == 0) {
            this.state = 1;
            this.ui.dd.multiSelect.show();
        } else {
            this.state = 0;
            this.ui.dd.multiSelect.hide();
        }

        this.ui.titleArrow.toggleClass("arrow-down");
        this.ui.titleArrow.toggleClass("arrow-left");
        if (this.state == 1 && this.search.enable) {
            this.ui.dd.search.string.focus();
        }
    }

    closeDropdown() {
        if (this.state == 1)
            this.toggleDropdown();
    }

    openDropdown() {
        if (this.state == 0)
            this.toggleDropdown();
    }

    setSelectionByIds(uqIds, disableTriggerEvent) {
        // это массив
        if (Object.prototype.toString.call(uqIds) === '[object Array]') {
            for (var i = 0; i < uqIds.length; i++) {
                // пытаемся найти совпадения по id
                var index = -1;
                for (var j = 0; j < this.items.length; j++) {
                    if (this.items[j][this.uniqueFieldName] == uqIds[i]) {
                        index = j;
                        break;
                    }
                }
                if (index != -1) {
                    this.setSelectedMulti(this.items[index], true, disableTriggerEvent);
                }
            }
        } else {
            var index = -1;
            for (var j = 0; j < this.items.length; j++) {
                if (this.items[j][this.uniqueFieldName] == uqIds) {
                    index = j;
                    break;
                }
            }
            if (index != -1) {
                if (this.monoMode) {
                    this.setSelectedMono(this.items[index], disableTriggerEvent);
                } else {
                    this.setSelectedMulti(this.items[index], true, disableTriggerEvent);
                }
            }
        }
    }

    setSelection(items) {
        // это массив
        if (Object.prototype.toString.call(items) === '[object Array]') {
            for (var i = 0; i < items.length; i++) {
                // пытаемся найти совпадения
                var index = -1;
                for (var j = 0; j < this.selectedItems.length; j++) {
                    if (this.selectedItems[j][this.uniqueFieldName] == items[i][this.uniqueFieldName]) {
                        index = j;
                        break;
                    }
                }
                if (index != -1)
                    continue;
                this.setSelectedMulti(items[i], true);
            }
        } else {
            var index = -1;
            for (var j = 0; j < this.selectedItems.length; j++) {
                if (this.selectedItems[j][this.uniqueFieldName] == items[this.uniqueFieldName]) {
                    index = j;
                    break;
                }
            }
            if (index != -1)
                return;
            if (this.monoMode) {
                this.setSelectedMono(items);
            } else {
                this.setSelectedMulti(items, true);
            }
        }
    }

    setItems(items) {
        this.items = items;
        this.createItems(false);
    }

    addItems(items) {
        // это массив
        if (Object.prototype.toString.call(items) === '[object Array]') {
            for (var i = 0; i < items.length; i++) {
                this.items.push(items[i]);
            }
        } else {
            this.items.push(items);
        }
        this.createItems(false);
    }

    clearSelection(disableTriggerEvent) {
        var copy = this.selectedItems.slice();
        for (var i = 0; i < copy.length; i++) {
            this.setSelectedMulti(copy[i], false, disableTriggerEvent);
        }
    }

    selectAll(disableTriggerEvent) {
		var copy = this.items.slice();
		this.setSelection(copy, disableTriggerEvent);
	}

    destroy() {
        this.ui.empty();
    }
}