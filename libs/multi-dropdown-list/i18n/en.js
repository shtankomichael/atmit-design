﻿export default  {
    'nothingSelected': 'Nothing selected',
    'search': 'Search',
    'nothingFound': 'Nothing found'
};