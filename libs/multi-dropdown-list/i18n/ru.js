﻿export default  {
    'nothingSelected': 'Ничего не выбрано',
    'search': 'Поиск',
    'nothingFound': 'Ничего не найдено'
};