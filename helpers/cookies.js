﻿export default class Cookies {
    static get(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }
    static set(name, value, props) {
        props = props || {}
        var exp = props.expires;

        if (typeof exp == 'number' && exp) {
            const d = new Date();
            d.setTime(d.getTime() + exp * 1000);
            exp = d;
        }

        if (exp && exp.toUTCString)
            props.expires = exp.toUTCString();

        if (props.path == null)
            props.path = '/';

        value = encodeURIComponent(value);
        var updatedCookie = `${name}=${value}`;
        for (let propName in props) {
            updatedCookie += `; ${propName}`;
            const propValue = props[propName];
            if (propValue !== true)
                updatedCookie += `=${propValue}`;
        }
        document.cookie = updatedCookie;
    }
    static delete(name) {
        Cookies.set(name, '', { expires: -1, path: '/' });
    }
};