﻿export default class UrlHelper {
    static pushFilterToUrl(src, dst, exclude, include) {
        if (exclude == undefined)
            exclude = [];
        if (include == undefined)
            include = [];
        var url = document.location.href;
        for (var prop in src) {
            if (exclude.indexOf(prop) > -1) {
                url = UrlHelper.removeParameter(prop, url);
                continue;
            }
            var srcProp = src[prop];
            var dstProp = dst[prop];
            if (include.indexOf(prop) > -1) {
                url = UrlHelper.addOrChangeParameterValue(prop, dstProp, url);
                continue;
            }
            if (srcProp != dstProp) {
                if (dstProp == null || dstProp == undefined || dstProp === "")
                    url = UrlHelper.removeParameter(prop, url);
                else
                    url = UrlHelper.addOrChangeParameterValue(prop, dstProp, url);
            } else {
                url = UrlHelper.removeParameter(prop, url);
            }
        }

        //удаляем последние ? или &
        if (url[url.length - 1] == '?' || url[url.length - 1] == '&') {
            url = url.substring(0, url.length - 1);
        }
        history.pushState({}, null, url);
    }
    static addOrChangeParameterValue(_paramName, _newValue, _url) {
        var url = _url == undefined ? document.location.href : encodeURI(_url);
        if (UrlHelper.isParameterExist(_paramName, url)) {
            var pattern = new RegExp('(' + _paramName + '=).*?(&|$)');
            return encodeURI(url.replace(pattern, '$1' + _newValue + '$2'));
        } else {
            if (_url != undefined) {
                return UrlHelper.addParameterValue(_paramName, _newValue, _url);
            }
            return UrlHelper.addParameterValue(_paramName, _newValue);
        }
    }
    static isParameterExist(_paramName, _url) {
        var pattern = new RegExp('(' + _paramName + '=).*?(&|$)');
        var url = _url == undefined ? document.location.href : encodeURI(_url);
        return url.search(pattern) >= 0;
    }
    static addParameterValue(_paramName, _value, _url) {
        var url = _url == undefined ? document.location.href : encodeURI(_url);
        if (!UrlHelper.isParameterExist(_paramName)) {
            url += (url.split('?')[1] ? '&' : '?') + _paramName + "=" + _value;
        }
        return encodeURI(url);
    }
    static removeParameter(_paramName, _url) {
        var url = _url == undefined ? document.location.href : _url;
        if (UrlHelper.isParameterExist(_paramName)) {
            var urlparts = url.split('?');
            if (urlparts.length >= 2) {
                var urlBase = urlparts.shift();
                var queryString = urlparts.join("?");

                var prefix = encodeURIComponent(_paramName) + '=';
                var pars = queryString.split(/[&;]/g);
                for (var i = pars.length; i-- > 0;)
                    if (pars[i].lastIndexOf(prefix, 0) !== -1)
                        pars.splice(i, 1);
                if (pars.length == 0) {
                    url = urlBase;
                } else {
                    url = urlBase + '?' + pars.join('&');
                }
            }
        }
        return encodeURI(url);
    }
}