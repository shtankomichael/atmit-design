﻿export default class HtmlFilter {
    static parse(html) {
        const el = document.createElement('div');
        el.innerHTML = html;
        const str = HtmlFilter.enumChildNodes(el).innerHTML
            .toString().replace(/</gi, '{{{').replace(/>/gi, '}}}');
        return str;
    }

    static enumChildNodes(node) {
        const tagFilter = new Array('applet', 'area', 'base', 'basefont', 'body', 'button', 'embed', 'form', 'frame', 'frameset', 'head', 'html', 'input', 'isindex', 'label', 'link', 'map', 'meta', 'noframes', 'noscript', 'object', 'optgroup', 'option', 'param', 'select', 'textarea', 'title', 'iframe', 'script'); // "style"

        if (node && node.nodeType === 1) {
            let child = node.firstChild;
            while (child) {
                const temp = child.nextSibling;
                if (child.nodeType === 1) {
                    if (tagFilter.indexOf(child.nodeName.toLowerCase()) > -1) {
                        node.removeChild(child);
                    } else {
                        for (let i = 0; i < child.attributes.length; i++) {
                            const attrName = child.attributes[i].nodeName.toLowerCase();
                            if (attrName.substring(0, 2) === 'on' && attrName !== 'onclick') {
                                i--;
                                child.removeAttribute(attrName);
                            }
                        }
                        if (child.firstChild != null)
                            HtmlFilter.enumChildNodes(child);
                    }
                }
                child = temp;
            }
        }
        return node;
    }
}