import History from './base';

import {
    cleanPath
} from './utils';

export default class HTML5History extends History {
    constructor(base) {
        super(base);

        const initLocation = this.getLocation();
        window.addEventListener('popstate', e => {
            const current = this.current;

            // Avoiding first `popstate` event dispatched in some browsers but first
            // history route not updated since async guard at the same time.
            const location = this.getLocation();
            if (this.current === null && location === initLocation)
                return;

            this.transitionTo(`${this.base}/${location}`);
        });
    }

    go(n) {
        window.history.go(n);
    }

    push(location) {
        location = cleanPath(this._buildUrl(location));
        this.transitionTo(location, true);
    }

    replace(location) {
        location = cleanPath(this._buildUrl(location));
        this.transitionTo(location, false);
    }

    changeUrl(location) {
        location = cleanPath(this._buildUrl(location));
    }

    _buildUrl(location) {
        if (location.indexOf(this.base) === 0)
            return location;
        return location.indexOf('/') === 0 ?
            this.base + location :
            this.base + '/' + location;
    }

    /**
     * @param {String} base 
     * @returns {String}
     */
    getLocation(loc = null) {
        let path = loc || (window.location.pathname + window.location.search + window.location.hash);
        if (this.base && path.indexOf(this.base) === 0)
            path = path.slice(this.base.length);
        if (path.indexOf('/') === 0)
            path = path.slice(1);
        return path;
    }
}