import {
    pushState,
    replaceState
} from './utils';
import EventEmitter from '../../helpers/event-emitter';

/**
 * History API wrapper + notifier
 * listeners:
 * 1.navigation:before
 * 2.navigation:after
 */
export default class History extends EventEmitter {
    /**
     * 
     * @param {String} base prefixRoute
     */
    constructor(base = null) {
        super();
        this.base = base;
        this.current = null;
    }
    /**
     * @param {Number} n 
     */
    go(n) {
        throw new Error('Not implemented');
    }
    /**
     * push to url
     * @param {String} loc 
     */
    push(loc) {
        throw new Error('Not implemented');
    }
    /**
     * 'silence' push
     * @param {String} loc
     */
    replace(loc) {
        throw new Error('Not implemented');
    }
    getLocation(loc) {
        throw new Error('Not implemented');
    }

    transitionTo(loc, push) {
        this.emit('navigating:before', {
            current: this.current,
            new: loc
        });
        push ? pushState(loc, false) : replaceState(loc);
        this.updateRoute(loc);
        this.emit('navigating:after', {
            current: this.current
        });
    }

    updateRoute(loc) {
        this.current = this.getLocation(loc);
    }
}