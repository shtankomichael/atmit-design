﻿export default class DisposableStack {
    constructor() {
        this.stack = [];
    }

    get length() {
        return this.stack.length;
    }

    push(func) {
        if (typeof func != 'function')
            throw new Error('func is not a function');
        this.stack.push(func);
    }

    pop() {
        return this.stack.pop();
    }

    dispose() {
        while (this.stack.length > 0)
            this.stack.pop()();
    }
}