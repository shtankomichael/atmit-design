﻿export default class UndoRedoManager {
    constructor() {
        this._commands = [];
        this._index = -1;
        this._limit = 0;
        this.isExecuting = false;
    }

    _execute(command, action) {
        if (!command || typeof command[action] !== "function") {
            return this;
        }
        this.isExecuting = true;

        command[action]();

        this.isExecuting = false;
        return this;
    }

    /*
           Add a command to the queue.
           */
    add(command) {
        if (this.isExecuting) {
            return this;
        }
        // if we are here after having called undo,
        // invalidate items higher on the stack
        this._commands.splice(this._index + 1, this._commands.length - this._index);

        this._commands.push(command);

        // if limit is set, remove items from the start
        if (this._limit && this._commands.length > this._limit) {
            this._removeFromTo(this._commands, 0, -(this._limit + 1));
        }

        // set the current this._index to the end
        this._index = this._commands.length - 1;
        if (this.callback) {
            this.callback();
        }
        return this;
    }

    /*
    Perform undo: call the undo function at the current index and decrease the index by 1.
    */
    undo() {
        var command = this._commands[this._index];
        if (!command) {
            return this;
        }
        this._index -= 1;
        this._execute(command, "undo");
        if (this.callback) {
            this.callback();
        }
        return this;
    }

    /*
    Perform redo: call the redo function at the next index and increase the index by 1.
    */
    redo() {
        var command = this._commands[this._index + 1];
        if (!command) {
            return this;
        }
        this._index += 1;
        this._execute(command, "redo");
        
        if (this.callback) {
            this.callback();
        }
        return this;
    }

    /*
    Clears the memory, losing all stored states. Reset the index.
    */
    clear() {
        var prev_size = this._commands.length;

        this._commands = [];
        this._index = -1;

        if (this.callback && (prev_size > 0)) {
            this.callback();
        }
    }

    hasUndo() {
        return this._index !== -1;
    }

    hasRedo() {
        return this._index < (this._commands.length - 1);
    }

    get command() {
        return this._commands;
    }

    get index() {
        return this._index;
    }

    set limit(l) {
        this._limit = l;
    }

    /*
    Pass a function to be called on undo and redo actions.
    */
    set callback(callbackFunc) {
        this.callback = callbackFunc;
    }

    _removeFromTo(array, from, to) {
        array.splice(from,
            !to ||
            1 + to - from + (!(to < 0 ^ from >= 0) && (to < 0 || -1) * array.length));
        return array.length;
    }
}