﻿import { Logger, Cookies } from '../index';

export default class SpeedTester {
    constructor() {
        this.url = 'https://s3.eu-west-1.amazonaws.com/atmonline/speedtest_file.jpg';
    }
    /**
    * Проверка доступности сервера
    * @returns {Promise} Результат промиса представляет статистику подключения к серверу
    */
    test() {
        return new Promise((resolve, reject) => {
            this._calcConnectionSpeed(`${this.url}?ticks=${new Date().getTime()}`)
                .then(stats => {
                    resolve(stats);
                }, e => {
                    Logger.log(`[SpeedTester] error: ${e}`);
                    reject(e);
                })
        });
    }
    _downloadFile(url) {
        return new Promise((resolve, reject) => {
            let startTime = 0, //начало запроса
                endTime = 0; //конец ответа

            let timeout = 10000;
            let timer = null;
            let isCancelled = false;

            var xhr = new XMLHttpRequest();

            xhr.open('GET', url, true);
            xhr.overrideMimeType('text/plain; charset=x-user-defined');

            xhr.onreadystatechange = function () {
                if (isCancelled)
                    return;

                if (xhr.readyState == 4) {
                    endTime = window.performance.now();
                    if (xhr.status == 200) {
                        resolve({
                            duration: endTime - startTime,
                            size: xhr.response.length
                        });
                    } else {
                        if (xhr.responseText == '') {
                            reject(`Response is empty. Url: ${url} | Status: ${xhr.status} | ReadyState: ${xhr.readyState} | endTime: ${endTime}`);
                        } else {
                            reject(xhr.responseText);
                        }
                    }
                }
            };
            xhr.onerror = function () {
                reject({
                    response: xhr.responseText,
                    status: xhr.status
                });
            };
            startTime = window.performance.now();

            try {
                xhr.send();
            } catch (e) {
                reject(e);
            }

            //заканчиваем 
            timer = setTimeout(() => {
                isCancelled = true;
                endTime = window.performance.now();
                xhr.abort();
                resolve({
                    duration: endTime - startTime,
                    size: 0
                });
            }, timeout);
        });
    }
    _calcConnectionSpeed(url) {
        return new Promise((resolve, reject) => {
            return this._downloadFile(url).then(fileRes => {
                let result = {
                    size: fileRes.size,
                    speedBps: 0,
                    speedKbps: 0,
                    speedMbps: 0,
                    durationMs: fileRes.duration
                };
                if (fileRes.size == 0)
                    resolve(result);

                let durationInSec = fileRes.duration / 1000;
                let bitsLoaded = fileRes.size * 8;

                result.speedBps = bitsLoaded / durationInSec;
                result.speedKbps = result.speedBps / 1024;
                result.speedMbps = result.speedKbps / 1024;
                resolve(result);
            }, reject);
        });
    }
}