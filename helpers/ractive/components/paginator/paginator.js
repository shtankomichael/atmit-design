import Ractive from 'ractive/runtime';
import template from './paginator.hbs';
import i18n from './i18n/i18n';

import './paginator.less';

export default Ractive.extend({
    template,
    isolated: false,
    i18n
});