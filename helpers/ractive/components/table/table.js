import Ractive from 'ractive/runtime';
import template from './table.hbs';
import i18n from './i18n/i18n';

export default Ractive.extend({
    template,
    isolated: false,
    i18n
});