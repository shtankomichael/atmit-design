import Ractive from 'ractive/runtime';
import DisposableStack from '../../disposable-stack';

export default Ractive.extend({
    on: {
        construct() {
            this.stack = new DisposableStack();
        },
        teardown() {
            this.stack.dispose();
        }
    }
});