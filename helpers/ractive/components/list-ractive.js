﻿import ViewRactive from './view-ractive';
import {
    Request
} from '../../../index';

import Table from './table/table';
import Paginator from './paginator/paginator';
import Col from './col/col';

const State = {
    Idle: 'idle',
    Downloading: 'downloading',
    Downloaded: 'downloaded'
};

export default ViewRactive.extend({
    data: () => ({
        getSortType(name) {
            if (name === this.get('filter.sortName'))
                return this.get('filter.sortType');
            return 'unsorted';
        },
        url: null,
        getItems(filter) {
            let url = this.get('url');
            if (url == null)
                throw new Error('url is undefined');
            return Request.post({
                url: url,
                data: filter
            });
        },
        isSelected(id = null, uniqueField = 'id') {
            if (id != null)
                return this.selectedItemsMap.has(id);
            return this.get('items').every(x => this.selectedItemsMap.has(x[uniqueField]));
        },
        ListState: State,
        state: State.Idle,
        items: null,
        pages: null,
        options: {},
        filter: {}
    }),
    selectedItemsMap: new Map(),
    components: {
        Table,
        Paginator,
        Column: Col
    },
    on: {
        init() {
            var items = this.get('items');
            if (items && items.length !== 0)
                this.calcPagination();
        },
        '*.changeSort' (ctx, name) {
            const sortType = this.get('filter.sortType') === 'asc' ? 'desc' : 'asc';
            this.set('filter.sortType', sortType);
            this.set('filter.sortName', name);
            this.set('filter.sort', name + ':' + sortType);
            this.set('filter.page', 1);
            this.updateItems();
        },
        deselectAll() {
            this.selectedItemsMap.clear();
            this.set('filter.selectedItems', []);
            this.set('filter.selectAll', false);
        },
        toggleAll(ctx, uniqueField = 'id') {
            let selectAll = this.get('filter.selectAll');
            this.selectedItemsMap.clear();
            if (!selectAll) {
                for (let item of this.get('items')) {
                    this.selectedItemsMap.set(item[uniqueField], true);
                }
            }
            this.set('filter.selectedItems', Array.from(this.selectedItemsMap.keys()));
            this.set('filter.selectAll', !selectAll);
        },
        toggleItem(ctx, id) {
            if (this.selectedItemsMap.get(id)) {
                this.selectedItemsMap.delete(id);
            } else {
                this.selectedItemsMap.set(id, true);
            }
            this.set('filter.selectedItems', Array.from(this.selectedItemsMap.keys()));
            this.set('filter.selectAll', false);
        },
        search(ctx) {
            this.set('filter.page', 1);
            this.updateItems();
        },
        //*. нужна для вызова функций парента из чайлд-компонента Paginator
        '*.changeValue' (ctx, name, value) {
            this.set('filter.' + name, value);
            this.set('filter.page', 1);
            this.updateItems();
        },
        '*.selectPage' (ctx, page) {
            if (page <= 0 || page > this.get('filter.pageTotal') || page === this.get('filter.page'))
                return;
            this.set('filter.page', page);
            this.updateItems();
        }
    },
    calcPagination() {
        const filter = this.get('filter');
        const pages = [];
        //1,2,3,4,5
        if (filter.pageTotal < 6) {
            for (var i = 1; i <= filter.pageTotal; i++) {
                pages.push({
                    num: i,
                    active: filter.page == i
                });
            }
        } else {
            //1,2,3,4,10
            if (filter.page < 4) {
                for (let i = 1; i <= 4; i++) {
                    pages.push({
                        num: i,
                        active: filter.page == i
                    });
                }
                pages.push({
                    num: filter.pageTotal
                });
                //1,5,6,7,10
            } else if (filter.page >= 4 && filter.page < filter.pageTotal - 1) {
                pages.push({
                    num: 1
                });
                for (let i = filter.page - 1; i <= filter.page + 1; i++) {
                    pages.push({
                        num: i,
                        active: filter.page == i
                    });
                }
                pages.push({
                    num: filter.pageTotal
                });
                //1,7,8,9,10
            } else {
                pages.push({
                    num: 1
                });
                for (let i = filter.pageTotal - 3; i <= filter.pageTotal; i++) {
                    pages.push({
                        num: i,
                        active: filter.page == i
                    });
                }
            }
        }
        this.set('pages', pages);
    },
    updateItems() {
        return new Promise((resolve, reject) => {
            const getItems = this.get('getItems');
            if (!getItems) {
                console.error('getItems is undefined');
                reject();
            }
            this.set('state', State.Downloading);
            getItems(this.get('filter')).then(res => {
                this.set('filter', res.filter);
                this.set('items', res.items);
                this.calcPagination();
                this.set('state', State.Downloaded);
                resolve();
            }, reject);
        });
    },
});