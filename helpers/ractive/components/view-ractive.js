import Validator from '../validation/validator';
import StackRactive from './stack-ractive';

export default StackRactive.extend({
    data: () => ({
        _page: {
            id: null,
            title: null
        }
    }),
    on: {
        init() {
            let page = this.get('_page');
            if (page.id != null)
                window[page.id] = this;
            this.options = page.options;
            if (page.isPartial)
                return;
                //TODO remove this global shit
            if (typeof (appRactive) != 'undefined')
                appRactive.set('globals._page', page);
        },
        teardown() {
            let page = this.get('_page');
            if (page.id != null && window[page.id] != null)
                delete window[page.id];
        }
    },
    changeTitle(title) {
        //TODO shit too
        if (typeof (appRactive) != 'undefined')
            appRactive.set('globals._page.title', title);
    },
    createValidator: function (basePath) {
        return new Validator(basePath, this);
    }
});