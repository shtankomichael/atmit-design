import Ractive from 'ractive/runtime';
import template from './col.hbs';

import './col.less';

export default Ractive.extend({
    template,
    data: () => ({
        setWidth() {
            let width = this.get('width');
            if (width == null)
                return null;
            return `width="${width}"`;
        }
    }),
    isolated: false,
});