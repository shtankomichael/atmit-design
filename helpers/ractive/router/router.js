import Ractive from 'ractive/runtime';
import i18n from './i18n/i18n';
import crossroads from 'crossroads';
import HTML5History from '../../history/html5';
import Alert from '../../alert';
import template from './router.hbs';

let RactiveRouter = Ractive.extend({
    template,
    on: {
        init () {
            //Will hold all constructed crossroads Route objects, for a further use I didn't find yet
            this.routesObjects = [];

            //The current componet the Router displays at a time
            this.currentComponent = undefined;
            //Get user config
            this.config = this.get('routesConfig');
            //Register for every route, a callback that will display the matching Component
            Object.keys(this.config.routes).map((routePattern) => {
                let routeObject = crossroads.addRoute(routePattern, function () {
                    let routeConfig = this.config.routes[routePattern];
                    //Prepare Route callback, if applied
                    let callback = routeConfig.callback instanceof Function ? routeConfig.callback : undefined;
                    let prevPattern = RactiveRouter.route.pattern;
                    this.updateRoute(routePattern, arguments);
                    //Create the Route Component or just execute the Route Callback

                    if (routeConfig.component) {
                        //if last pattern was the same
                        if (prevPattern && prevPattern === routePattern) {
                            this.currentComponent.update('globals.route');
                        } else {
                            if ((routeConfig.id == null || this.currentComponent == null) ||
                                routeConfig.id != null &&
                                this.currentComponent != null &&
                                routeConfig.id !== this.currentComponent.id) {
                                this.currentComponent = new routeConfig.component({
                                    el: this.find('main.routeContainer'),
                                    data: {
                                        globals: this.get('globals')
                                    },
                                    oncomplete() {
                                        if (callback) callback(route);
                                    }
                                });
                                this.currentComponent.id = routeConfig.id;
                            }

                        }
                    } else {
                        if (callback) callback(route);
                    }
                }.bind(this)); // BIND CONTEXT!!
                this.routesObjects.push(routeObject);
            });

            //Observe Global Data and notify current route Component
            this.observe('globals', function (globals) {
                if (this.currentComponent && this.currentComponent.update) {
                    this.currentComponent.update('globals');
                }
            });

            //Redirect Not found route to 404
            crossroads.bypassed.add(() => {
                Alert.showToastr(i18n[RactiveRouter.options.lang]['page404'], Alert.Type.Danger, `${i18n[RactiveRouter.options.lang]['attention']}!`,
                    {
                        preventDuplicates: false
                    });
                if (this.config.defaultRoute != null)
                    RactiveRouter.replace(this.config.defaultRoute);
                else
                    RactiveRouter.replace('/');
            });

            switch (this.config.mode) {
                case 'html5':
                    this.history = new HTML5History(this.config.baseUrl);
                    break;
                case 'hash':
                    //TODO this.history = new HashHistory();
                    break;
            }
            RactiveRouter.history = this.history;
            RactiveRouter.go = (delta) => {
                this.history.go(delta);
            };
            RactiveRouter.push = (url) => {
                this.history.push(encodeURI(url));
            };
            RactiveRouter.replace = (url) => {
                this.history.replace(encodeURI(url));
            };
        },
        render () {
            this.history.on('navigating:after', (route) => {
                crossroads.parse(route.current);
            });
            crossroads.parse(this.history.getLocation());
        }
    },
    buildRouteParams(routePattern, values) {
        let routeParamNames = crossroads.patternLexer.getParamIds(routePattern);
        let result = routeParamNames.reduce((result, field, index) => {
            let value = values[index];
            if (typeof values[index] !== 'object') {
                result[field] = values[index] || undefined;
            } else {
                Object.keys(value).map((key) => {
                    result[key] = value[key];
                });
            }
            return result;
        }, {});
        return result;
    },
    updateRoute(routePattern, args) {
        //Build a Route Params Object that will be available in child Component Data
        let routeParams = this.buildRouteParams(routePattern, args);

        //Route info
        RactiveRouter.route.pattern = routePattern;
        RactiveRouter.route.loc = this.history.getLocation();
        RactiveRouter.route.params = routeParams;
    }
});

/**
 * @description Options
 */
RactiveRouter.options = {
    lang: 'ru'
};

/**
 * @description Back or Next of browser history
 */
RactiveRouter.go = (delta) => { };
/**
 * @description  Programatically navigate to a new route
 */
RactiveRouter.push = (url) => { };
/**
 * @description Similar to Router.go(hash), but doesn't create a new record in browser history
 */
RactiveRouter.replace = (url) => { };
/**
 * @description Current route information (immutable)
 */
RactiveRouter.route = {
    pattern: null,
    params: null,
    loc: ''
};
/**
 * @description History
 */
RactiveRouter.history = null;

export default RactiveRouter;