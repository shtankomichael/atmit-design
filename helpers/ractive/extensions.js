﻿import Language from '../../enums/language';
import Ractive from 'ractive/runtime';
import Mask from '../../enums/mask';
import Events from './events/events';
import Decorators from './decorators/index';

Object.assign(Ractive.defaults.data, {
    Lang: Language.Ru,
    Mask: Mask,
    toFixed(number, count) {
        if (number === 0 || number == null || count == null)
            return 0;
        return number.toFixed(count);
    },
    getDate(date, format) {
        return Date.format(date, format);
    },
    getTimeFromSec(sec) {
        return Date.getTimeFromSec(sec);
    },
    declOfNum(number, stringArray) {
        return (number).declOfNum(stringArray);
    },
    loc(key) {
        if (key == null)
            return 'Key is empty, please check key';
        if (this.i18n == null)
            return '::key::';
        return this.i18n[this.get('Lang')][key];
    }
});

Object.assign(Ractive.decorators, {
    alert: Decorators.Alert,
    datepicker: Decorators.Datepicker,
    preloader: Decorators.Preloader,
    mask: Decorators.Mask,
    route: Decorators.Route,
    contextmenu: Decorators.Contextmenu,
    dropdown: Decorators.Dropdown,
    'item-selector': Decorators.ItemsSelector,
    'drag-scroll': Decorators.DragScroll,
    'validation': Decorators.Validation,
    'validation-summary': Decorators.ValidationSummary,
    'sort': Decorators.Sort,
    'sortable': Decorators.Sortable
});

Object.assign(Ractive.events, {
    enter: Events.Enter,
    dblclick: (node, fire) => Events.EventListener(node, fire, 'dblclick'),
    dragstart: (node, fire) => Events.EventListener(node, fire, 'dragstart'),
    drop: (node, fire) => Events.EventListener(node, fire, 'drop'),
    dragover: (node, fire) => Events.EventListener(node, fire, 'dragover'),
    dragend: (node, fire) => Events.EventListener(node, fire, 'dragend'),
    inputdelay: Events.InputDelay,
    resize: (node, fire) => Events.EventListener(node, fire, 'resize')
});

/**
 * Render ractive component as isolated partial view
 */
Function.prototype.on = function (events) {
    return this.extend({
        on: events
    });
};

/**
 * Set custom options of a component
 */
Function.prototype.withOptions = function (options) {
    return this.extend({
        on: {
            config() {
                this.set('_page.options', options);
            }
        }
    });
};