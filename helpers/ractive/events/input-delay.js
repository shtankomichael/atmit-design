export default (node, fire) => {
    let delay = node.hasAttribute('data-delay')
        ? node.getAttribute('data-delay')
        : 500;

    let timer = null;
    const keyup = function (e) {
        clearTimeout(timer);
        timer = setTimeout(() => {
            fire({
                node: node,
                original: e
            });
        }, delay);
    };
    node.addEventListener('keyup', keyup);
    return {
        teardown() {
            node.removeEventListener('keyup', keyup);
        }
    };
};