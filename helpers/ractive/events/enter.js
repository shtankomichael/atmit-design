export default (node, fire) => {
    const keyup = function (e) {
        if (e.which === 13 && !e.shiftKey) {
            fire({
                node: node,
                original: e
            });
        }
    };
    node.addEventListener('keyup', keyup);
    return {
        teardown() {
            node.removeEventListener('keyup', keyup);
        }
    };
};