export default (node, fire, eventName) => {
    const f = function (e) {
        fire({
            node: node,
            original: e
        });
    };
    node.addEventListener(eventName, f);
    return {
        teardown() {
            node.removeEventListener(eventName, f);
        }
    };
};