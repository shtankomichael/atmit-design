export default function (node, sortName, sortAction = 'changeSort') {
    let ractive = this;
    const click = function (e) {
        e.preventDefault();
        e.stopPropagation();
        ractive.fire(sortAction, null, sortName);
    };
    node.addEventListener('click', click);
    return {
        teardown() {
            node.removeEventListener('click', click);
        }
    };
};