import { Preloader } from '../../../index';

export default (node, isRunning, text, isFixed) => {
    let loader;

    var createLoader = (text, isFixed) => {
        loader = Preloader.show({
            node,
            text,
            isFixed
        });
    };

    var removeLoader = () => {
        if (loader == null)
            return;
        loader.hide();
        loader = null;
    };

    if (isRunning)
        createLoader(text, isFixed);

    return {
        update: (isRunning, text, isFixed) => {
            if (isRunning) {
                removeLoader();
                createLoader(text, isFixed);
            } else
                removeLoader();
        },
        teardown: () => {
            removeLoader();
        }
    };
};