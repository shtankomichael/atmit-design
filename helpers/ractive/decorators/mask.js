import Inputmask from 'inputmask';

export default function(node, mask){
    Inputmask({
        regex: mask
    }).mask(node);
    return {
        teardown() {}
    };
};