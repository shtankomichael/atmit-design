import { Router } from '../../../index';

export default (node, push = true, attr = null) => {
    const click = function (e) {
        e.preventDefault();
        // e.stopPropagation();

        const url = node.getAttribute(attr || 'href');
        if (push) Router.push(url);
        else Router.replace(url);
    };
    node.addEventListener('click', click);
    return {
        teardown() {
            node.removeEventListener('click', click);
        }
    };
};