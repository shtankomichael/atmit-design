export default function (node) {
    let pushed = false,
        moving = false,
        timer,
        newScrollX,
        newScrollY,
        lastClientX,
        lastClientY;

    let mousemove = function (e) {
        if (pushed) {
            moving = moving || Math.abs(lastClientX - e.clientX) > 0.5 || Math.abs(lastClientY - e.clientY) > 0.5;
            if (moving) {
                node.scrollLeft -= newScrollX = (-lastClientX + (lastClientX = e.clientX));
                node.scrollTop -= newScrollY = (-lastClientY + (lastClientY = e.clientY));
            }
        }
    };

    let mousedown = (e) => {
        pushed = true;
        lastClientX = e.clientX;
        lastClientY = e.clientY;
        e.preventDefault();
        document.addEventListener('mousemove', mousemove);
    };
    node.addEventListener('mousedown', mousedown);

    let mouseup = (e) => {
        pushed = false;
        document.removeEventListener('mousemove', mousemove);
        timer = setTimeout(() => {
            moving = false
        }, 10);
    };
    document.addEventListener('mouseup', mouseup);

    let click = (e) => {
        if (moving) {
            clearTimeout(timer);
            moving = false;
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
    };
    document.body.addEventListener('click', click, true);

    return {
        teardown() {
            node.removeEventListener('mousedown', mousedown);
            document.removeEventListener('mouseup', mouseup);
            document.removeEventListener('mousemove', mousemove);
            document.body.removeEventListener('click', click);
        }
    };
};