import './dropdown.less'

const defaultDropdownOptions = {
    autoClose: false,
    onShown() {},
    onClosed() {}
};

export default (node, options = {}) => {
    node.classList.add('dd-menu');
    let content = node.querySelector('.dd-content');
    content.classList.add('hidden');
    Object.merge(options, defaultDropdownOptions);    
    const toggle = function (node) {
        if (!content.classList.contains('hidden')) {
            content.classList.add('hidden');            
            options.onClosed();
        }
        else {
            content.classList.remove('hidden');            
            options.onShown();
        }
    };

    const f = function (e) {     
        if (e.target.closest('.dd-content') == null)
            toggle(); 
    };
    const onClickOutside = function (e) {
        var isClickInside = node.contains(e.target);
        if (!isClickInside)
            isClickInside = !document.contains(e.target);
        if (!isClickInside && !content.classList.contains('hidden')) {
            content.classList.add('hidden');
            options.onClosed();
        }
    };

    node.addEventListener('click', f);
    document.addEventListener('click', onClickOutside);
    return {
        teardown() {
            node.removeEventListener('click', f);
            document.addEventListener('click', onClickOutside);
        }
    };
};