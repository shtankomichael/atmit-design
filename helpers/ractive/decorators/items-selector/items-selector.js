import Scroll from '../../../scroll';
import './items-selector.less';

export default (node) => {

    node.classList.add('items-selector');
    let left = node.getElementsByClassName('arrow-left')[0];
    let right = node.getElementsByClassName('arrow-right')[0];
    let itemsBlock = node.querySelector('.items');

    const leftClick = function (e) {
        e.preventDefault();
        e.stopPropagation();
        Scroll.toLeft(itemsBlock);
    };
    left.addEventListener('click', leftClick);

    const rightClick = function (e) {
        e.preventDefault();
        e.stopPropagation();
        Scroll.toRight(itemsBlock);
    };
    right.addEventListener('click', rightClick);
    return {
        teardown() {
            left.removeEventListener('click', leftClick);
            right.removeEventListener('click', rightClick);
        }
    };
};