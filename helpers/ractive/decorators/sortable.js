import Ractive from 'ractive/runtime';

const defaultSortableOptions = {
    targetClass: 'drop-target',
    errorMessage: 'The sortable decorator only works with elements that correspond to array members',
    on: {
        dragStart(node, event) {

        },
        dragEnter(node, event) {

        },
        dragLeave(node, event) {

        },
        dragOver(node, event) {

        },
        drop(node, event) {

        }
    }
};

export default function (node, options = {}) {
    Object.merge(options, defaultSortableOptions);    

    var ractive,
        sourceKeypath,
        sourceArray;

    let dragStartHandler = function (event) {
        options.on.dragStart(node, event);
        var context = Ractive.getContext(this);

        sourceKeypath = context.resolve();
        sourceArray = context.resolve('../');

        if (!Array.isArray(context.get('../'))) {
            throw new Error(options.errorMessage);
        }

        event.dataTransfer.setData('foo', true); // enables dragging in FF. go figure

        // keep a reference to the Ractive instance that 'owns' this data and this element
        ractive = context.ractive;
    };
    let dragEnterHandler = function (event) {
        options.on.dragEnter(node, event);

        var targetKeypath, targetArray, array, source, context;

        context = Ractive.getContext(this);

        // If we strayed into someone else's territory, abort
        if (context.ractive !== ractive) {
            return;
        }
        targetKeypath = context.resolve();
        targetArray = context.resolve('../');

        // if we're dealing with a different array, abort
        if (targetArray !== sourceArray) {
            return;
        }

        // if it's the same index, add droptarget class then abort
        if (targetKeypath === sourceKeypath)
            return;
        // remove source from array
        source = ractive.get(sourceKeypath);
        array = Ractive.splitKeypath(sourceKeypath);

        ractive.splice(targetArray, array[array.length - 1], 1);

        // the target index is now the source index...
        sourceKeypath = targetKeypath;

        array = Ractive.splitKeypath(sourceKeypath);

        // add source back to array in new location
        ractive.splice(targetArray, array[array.length - 1], 0, source);
    };
    let dragLeaveHandler = function (event) {        
        options.on.dragLeave(node, event);     
        this.classList.remove(options.targetClass);
    };    
    let dragOverHandler = function (event) {
        options.on.dragOver(node, event);
        if (!this.classList.contains(options.targetClass)) {
            this.classList.add(options.targetClass);
        }
        event.preventDefault();
    };
    let dropHandler = function (event) {
        options.on.drop(node, event);
        this.classList.remove(options.targetClass);
    };

    node.draggable = true;

    node.addEventListener('dragstart', dragStartHandler, false);
    node.addEventListener('dragenter', dragEnterHandler, false);
    node.addEventListener('dragleave', dragLeaveHandler, false);    
    node.addEventListener('dragover', dragOverHandler, false);   
    node.addEventListener('drop', dropHandler, false);

    return {
        teardown() {
            node.removeEventListener('dragstart', dragStartHandler, false);
            node.removeEventListener('dragenter', dragEnterHandler, false);
            node.removeEventListener('dragleave', dragLeaveHandler, false);            
            node.removeEventListener('dragover', dragOverHandler, false);
            node.removeEventListener('drop', dropHandler, false);
        }
    };
};