﻿export default (node, fieldValidationObject) => {
    node.classList.add('alert');
    node.classList.add('alert-danger');
    
    let ulElement = null;

    var createListElement = function (error) {
        const liElement = document.createElement('li');
        liElement.innerHTML = error;
        return liElement;
    }

    var checkErrorExist = function (node, fieldValidationObject) {
        
        if (fieldValidationObject != null && fieldValidationObject.hasError) {
            
            node.innerHTML = '';
            node.classList.remove('d-none');
          
            const ul = document.createElement('ul');
            ul.classList.add('mb-0');
            for (let error of fieldValidationObject.errors) {
                ul.appendChild(createListElement(error));
            }
            node.appendChild(ul);
            ulElement = ul;
        } else {
            if (ulElement != null) {
                ulElement.remove();
                ulElement = null;
            }
            node.classList.add('d-none');
        }
    }

    checkErrorExist(node, fieldValidationObject);

    return {
        update: (fieldValidationObject) => {
            checkErrorExist(node, fieldValidationObject);
        },
        teardown: () => {
        }
    };
};