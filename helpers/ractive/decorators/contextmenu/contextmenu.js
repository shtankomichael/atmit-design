import './contextmenu.less';

export default function (node, items) {

    let menu = null;
    const clickOutside = function (e) {
        var isClickInside = menu.contains(e.target);
        if (!isClickInside)
            menu.remove();
    };

    const contextmenuHandler = (event) => {
        event.preventDefault();
        menu = document.getElementsByTagName("menu");

        document.addEventListener('click', clickOutside);

        if (menu.length > 0)
            menu.remove();
        if (items != null) {
            menu = document.createElement("menu");
            for (let item of items) {
                let menuitem = document.createElement("menuitem");
                menuitem.setAttribute("label", item.title);
                menuitem.addEventListener('click', (e) => {
                    e.preventDefault();
                    this.fire(item.event, this.getContext(node), item.data);
                });
                menu.appendChild(menuitem);
            }
            node.appendChild(menu);
        }
    };
    node.addEventListener('contextmenu', contextmenuHandler);
    return {
        teardown: function () {
            document.removeEventListener('click', clickOutside);
            node.removeEventListener('contexmenu', contextmenuHandler);
        }
    };
};