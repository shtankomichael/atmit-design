﻿export default (node, fieldValidationObject) => {

    let feedbackElements=[];

    var clearFeedbacks = function() {
        for (let feedback of feedbackElements)
            feedback.remove();
        feedbackElements = [];
    }

    var createFeedback = function(error) {
       const feedbackElement = document.createElement('div');
        feedbackElement.classList.add('invalid-feedback');
        node.parentNode.insertBefore(feedbackElement,node.nextSibling );
        feedbackElement.innerHTML = error;
        return feedbackElement;
    }

    var checkErrorExist = function (node, fieldValidationObject) {
        if (fieldValidationObject != null && fieldValidationObject.hasError) {
            clearFeedbacks();
            for (let error of fieldValidationObject.errors) {
                    feedbackElements.push(createFeedback(error));
            }
            node.classList.add('is-invalid');
        } else {
            clearFeedbacks();
            node.classList.remove('is-invalid');
        }
    }

    checkErrorExist(node, fieldValidationObject);

    return {
        update: (fieldValidationObject) => {
            checkErrorExist(node, fieldValidationObject);
        },
        teardown: () => {
        }
    };
};