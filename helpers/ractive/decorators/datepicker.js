import Flatpickr from "flatpickr";
import { Russian } from "flatpickr/dist/l10n/ru.js";

const defaultFlatpickrOptions = {
    dateFormat: "d.m.Y",
    defaultDate: new Date(),
    lang: 'ru'
};

export default (node, options) => {
    Object.merge(options, defaultFlatpickrOptions);
    options.locale = options.lang === 'ru' ? Russian : null;

    var picker = Flatpickr(node, options);
    return {
        teardown() {
            picker.destroy();
        }
    };
};