﻿import Rules from './rules/index';
export default class ValidatorRules {
    constructor() {
        //dictionary of private rules
        //you must add new rules here and in static enum ValidatorRules.Rules bellow
        this._rules = {
            require: Rules.Require,
            maxLength: Rules.MaxLength
        };
    }

    get(ruleName) {
        if (this._rules[ruleName] == null)
            throw 'no such rule found';
        return this._rules[ruleName];
    }
}
ValidatorRules.Rules = {
    Require: 'require',
    MaxLength:'maxLength'
}