﻿import ValidatorRules from './validator-rules';
/**
 * To create instance user this.createValidator(basePath) from page ractive instance
 * then use set rules method for client validation rules
 * to run client validation call validate method
 * to process server validation call setModelState method
 */
export default class Validator {
    constructor(basePath, ractive) {
        if (basePath == null || basePath.length === 0)
            throw "basePath not defined";
        //ractive model base keypass
        this._basePath = basePath;
        this._ractive = ractive;
        //dictionary of current errors
        this._existingErrorsInfo = {};
        //dictionary of current rules
        this._rules = {};
        this._validatorRules = new ValidatorRules();
    }

    /**
     * Set and render model errors
     * @param {{}} modelStateErrors
     */
    setModelState(modelStateErrors) {
        this.clearModelState();
        if (modelStateErrors == null)
            return;

        const errorFieldPrefix = '_';
        const summaryErrorField = `${errorFieldPrefix}summary`;
        //base ractive object, need to store summary errors
        const baseObject = this._ractive.get(this._basePath);
        for (let modelStateError of modelStateErrors) {
            let objectPath = this._basePath;
            let errorField = null;
            //if key of error is empty - add summary error
            if (modelStateError.key === '') {
                errorField = summaryErrorField;
                if (baseObject != null)
                    this._addSummaryError(errorField, baseObject, modelStateError.errors);
                else
                    console.warn(`${this._basePath} not found`);
            } else {
                //split key to find path of field parent
                const splittedKey = modelStateError.key.split('.');
                //finding parent paths with fragments starting from lower letters
                objectPath = this._getObjectPath(splittedKey);

                let fieldName = splittedKey[splittedKey.length - 1];
                fieldName = fieldName[0].toLowerCase() + fieldName.substr(1);

                const object = this._ractive.get(objectPath);
                //if field key exist add validation error
                if (object.hasOwnProperty(fieldName)) {
                    errorField = `${errorFieldPrefix}${fieldName}`;

                    if (object[errorField] == null)
                        Object.defineProperty(object, errorField, {
                            value: {
                                errors: modelStateError.errors,
                                hasError: true
                            },
                            enumerable: false,
                            configurable: true,
                            writable: true
                        });
                } else {
                    //add summary error
                    errorField = summaryErrorField;
                    this._addSummaryError(errorField, baseObject, modelStateError.errors);
                    console.warn(`${objectPath}.${fieldName} not found`);
                    //change base path, cause summary errors holds in base object
                    objectPath = this._basePath;
                }
            }


            if (this._existingErrorsInfo[objectPath] == null)
                this._existingErrorsInfo[objectPath] = [];

            this._existingErrorsInfo[objectPath].push({
                name: errorField
            });
        }

        if (modelStateErrors.length)
            this._ractive.update(this._basePath);
    }
    /**
     * Add summary error to object
     * @param {string} fieldName - field to add errors
     * @param {{}} object - object to add errors
     * @param {Array<string>} errors - array of field errors
     */
    _addSummaryError(fieldName, object, errors) {
        if (object[fieldName] == null)
            Object.defineProperty(object, fieldName, {
                value: {
                    errors: errors,
                    hasError: true
                },
                enumerable: false,
                configurable: true,
                writable: true
            });
        else
            object[fieldName].errors = object[fieldName].errors.concat(errors);
    }
    /**
     * Set rules to current validator
     * @param {Object<string,{}>} rules
     */
    setRules(rules) {
        for (let path in rules)
            if (rules.hasOwnProperty(path))
                this._rules[path] = rules[path];
    }
    /**
     * Runs client validation
     */
    validate() {
        const rules = this._rules;
        if (Object.keys(this._rules).length === 0)
            console.warn('use of validator validate method without rules');
        //dictionary to store validation errors
        const modelStateErrors = {};
        let isModelValid = true;
        for (let path in rules) {
            if (rules.hasOwnProperty(path)) {
                const pathRules = rules[path];
                const ractivePath = `${this._basePath}.${path}`;
                let isValid = false;
                //dilimeter to * wild card for array and dictionary
                if (ractivePath.includes('*')) {
                    const splittedPath = ractivePath.split('.*.');
                    const objectPaths = this._buildWildCardPaths(splittedPath);
                    for (let objectPath of objectPaths) {
                        const value = this._ractive.get(objectPath);
                        isValid = this._checkValue(value, objectPath, pathRules, modelStateErrors);
                        if (isModelValid)
                            isModelValid = isValid;
                    }
                } else {
                    const value = this._ractive.get(ractivePath);
                    isValid = this._checkValue(value, ractivePath, pathRules, modelStateErrors);
                    if (isModelValid)
                        isModelValid = isValid;
                }
            }
        }
        this.setModelState(Object.values(modelStateErrors));
        return isModelValid;
    }

    /**
     * Build paths of objects that corresponds wild card (*) mask
     * @param {string} splittedPath
     * @param {string} baseString
     * @param {number} depth
     */
    _buildWildCardPaths(splittedPath = '', baseString = '', depth = 0) {
        const pathHolder = {
            paths: []
        };
        const objectPath = splittedPath[depth];
        depth++;
        const object = this._ractive.get(objectPath);
        if (Array.isArray(object)) {
            for (let i = 0; i < object.length; i++) {
                let currentPath = '';
                if (baseString.length !== 0)
                    currentPath = `${baseString}.${objectPath}[${i}]`;
                else
                    currentPath = `${objectPath}[${i}]`;

                this._addWildCardPaths(pathHolder, splittedPath, currentPath, depth);
            }
        } else {
            for (let key in object) {
                if (object.hasOwnProperty(key)) {
                    let currentPath = '';
                    if (baseString.length !== 0)
                        currentPath = `${baseString}.${objectPath}.${i}`;
                    else
                        currentPath = `${objectPath}.${i}`;

                    this._addWildCardPaths(pathHolder, splittedPath, currentPath, depth);
                }
            }
        }
        return pathHolder.paths;
    }
    /**
     * add recursive call of  _buildWildCardPaths method
     * @param {{}} pathHolder object wrapper to hold paths array ref
     * @param {Array<string>} splittedPath
     * @param {string} currentPath
     * @param {number} depth
     */
    _addWildCardPaths(pathHolder, splittedPath, currentPath, depth) {
        //we need -1 to avoid endless loop cause depth start from 0 and when _buildWildCardPaths method calls we have at least 2 elements array
        if (depth !== splittedPath.length - 1) {
            //this case not tested, sorry
            pathHolder.paths = this._buildWildCardPaths(splittedPath, currentPath, depth);
        } else
            pathHolder.paths.push(`${currentPath}.${splittedPath[splittedPath.length - 1]}`);
    }

    /**
     * Check if object value valid 
     * @param {any} value cheking value can be null or undefined
     * @param {string} path path for modelStateErrors
     * @param {Array<{}>} pathRules array of rules for validation
     * @param {{}} modelStateErrors
     */
    _checkValue(value, path, pathRules, modelStateErrors) {
        let isValid = true;
        for (let rule in pathRules) {
            if (pathRules.hasOwnProperty(rule)) {
                const validatorRule = this._validatorRules.get(rule);
                //switch for correct method calls
                var result = validatorRule(value, pathRules[rule]);
                //if we have at least one error: value - invalid
                if (isValid)
                    isValid = result.isValid;
                if (!result.isValid) {
                    const errorMsg = pathRules[rule].message == null ? result.message : pathRules[rule].message;
                    if (modelStateErrors[path] == null) {
                        modelStateErrors[path] = {
                            key: path,
                            errors: []
                        };
                        modelStateErrors[path].errors.push(errorMsg);
                    } else {
                        modelStateErrors[path].errors.push(errorMsg);
                    }
                }
            }
        }
        return isValid;
    }
    /**
     * method returns path of field parent
     * @param {Array<string>} splittedKey splitted object key
     */
    _getObjectPath(splittedKey) {
        if (splittedKey.length > 1) {
            let objectPath = '';
            //-1 cause we don`t need field name
            for (let i = 0; i < splittedKey.length - 1; i++) {
                const keyPart = splittedKey[i];
                if (objectPath.length !== 0)
                    objectPath += '.';
                objectPath += keyPart[0].toLowerCase() + keyPart.substr(1);
            }
            if (objectPath.startsWith(this._basePath))
                return objectPath;
            return `${this._basePath}.${objectPath}`;
        }
        return this._basePath;
    }

    _clearModelState() {
        let hasChanges = false;
        const existingErrorsInfo = this._existingErrorsInfo;
        for (let parentObjectPath in existingErrorsInfo) {
            if (existingErrorsInfo.hasOwnProperty(parentObjectPath)) {
                hasChanges = true;
                const parentObject = this._ractive.get(parentObjectPath);
                const objectFields = existingErrorsInfo[parentObjectPath];

                for (let field of objectFields) {
                    delete parentObject[field.name];

                }
            }
        }
        if (hasChanges)
            this._existingErrorsInfo = {};
        return hasChanges;
    }

    clearModelState() {
        if (this._clearModelState())
            this._ractive.update(this._basePath);
    }
}


