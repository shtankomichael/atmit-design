﻿import I18n from './i18n/i18n'
import RuleResult from '../rule-result';

export default function (value, settings = { lang: 'ru' }) {
    if (settings.length == null)
        throw 'Length for max length rule not specified';

    if (value == null || value.length == null || value.length <= settings.length)
        return new RuleResult(true);

    let i18n = i18n[settings.lang];

    let symbolsWord = (settings.length).declOfNum([i18n['symbol'], i18n['symbol(2)'], i18n['symbol(5)']]).toLowerCase();
    const errorMessage = `${i18n['lengthShouldBeLessThan']} ${settings.length} ${symbolsWord}.`;

    return new RuleResult(false, errorMessage);
}