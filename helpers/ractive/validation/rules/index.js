﻿import Require from './require/require';
import MaxLength from './max-length/max-length';
export default {
    Require,
    MaxLength
}