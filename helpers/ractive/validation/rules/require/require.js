﻿import I18n from './i18n/i18n'
import RuleResult from '../rule-result';

export default function (value, settings = { lang: 'ru' }) {
    let message = I18n[settings.lang]['errorRequired'];
    if (settings.message != null)
        message = settings.message;
    if (value == null || value.length === 0)
        return new RuleResult(false, message);
    else
        return new RuleResult(true, message);
};