﻿//Here we can add function for message format,for example and store anything we need
export default class RuleResult {
    constructor(isValid, message) {
        if (!isValid && (message == null || message.length === 0))
            throw 'rule must return default message';
        this.isValid = isValid;
        this.message = message;
    }
}