import EventEmitter from './event-emitter';

class DispatcherModule extends EventEmitter {
    constructor() {
        super();
        this.data = {};
    }

    get(name) {
        if(name == null)
            return this.data;
        return this.data[name];
    }
    set(name, object) {
        this.data[name] = object;
    }
}

export default class Dispatcher extends DispatcherModule {
    constructor() {
        super();
    }

    /**
     * create or edit module
     * @param {string} name 
     */
    module(name) {
        if (this[name] != null)
            return this[name];
        this[name] = new DispatcherModule();
        return this[name];
    }
}