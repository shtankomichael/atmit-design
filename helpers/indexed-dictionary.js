﻿/**
 * Индексируемый словарь
 * Позволяет получать элемент как по ключу так и по индексу
 */
export default class IndexedDictionary {
	constructor(_arr, _key) {
		this.key = null;
		this.dict = {};
		this.elements = [];
		this.fillDict(_arr, _key);
	}
	get count() {
		return this.elements.length;
	}
	get lastIndex() {
		return this.elements.length - 1;
	}
	/**
	 * Очистка словаря
	 */
	clear() {
		this.key = null;
		this.dict = {};
		this.elements = [];
	}
	/**
	 * Заполняет словарь или перезаполняет если там уже были данные
	 * @param {any} _arr - исходный массив
	 * @param {any} _key - ключ по которому будет собран словарь
	 */
	fillDict(_arr, _key) {
		if (this.count !== 0) {
			this.clear();
		}
		this.key = _key;
		let index = 0;
		while (index < _arr.length) {
			this._initElement(_arr[index], _arr, index);
			index++;
		}
		this.elements = _arr;
	}
	/**
	 * Проверка на наличее ключа
	 * @param {string} _key
	 */
	hasKey(_key) {
		return this.dict[_key] != null;
	}
	/**
	 * Достает элемент по ключу
	 * @param {string} _key
	 */
	getByKey(_key) {
		return this.dict[_key];
	}
	/**
	 * Достает элемент по индексу
	 * @param {number} _index
	 */
	getByIndex(_index) {
		return this.elements[_index];
	}
	/**
	 * Удаляет элемент из словаря по индексу
	 * @param {number} _index
	 * @return {boolean} false - Элемент не найден, true - элемент успешно удален
	 */
	removeIndex(_index) {
		const element = this.elements[_index];
		if (element == null)
			return false;
		this._decreaseNextIndexes(element);
		if (_index !== 0)
			this.elements[_index - 1].__nextElement = this.elements[_index + 1];
		delete this.dict[element[this.key]];
		this.elements.splice(_index, 1);
		return true;
	}
	/**
	 * Удаляет элемент из словаря по ключу
	 * @param {string} _key
	 * @return {boolean} false - Элемент не найден, true - элемент успешно удален
	 */
	removeKey(_key) {
		const element = this.dict[_key];
		if (element == null)
			return false;
		this._decreaseNextIndexes(element);
		if (element.__index !== 0)
			this.elements[element.__index - 1].__nextElement = this.elements[element.__index + 1];
		delete this.dict[element[this.key]];
		this.elements.splice(element[this.key].__index, 1);
		return true;
	}
	/**
	 * Добавляет элемент в конец словаря
	 * если указан индекс то вставит элемент по этому индексу
	 * если такой элемент уже есть то производится мерж полей
	 * @param {{}} _element - объект с ключом словаря
	 * @param {number} _index
	 * @throws key not exest
	 * @return {number} - index
	 */
	add(_element, _index = null) {
		if (_element[this.key] == null)
			throw 'key not exist';
		if (!this.hasKey(_element[this.key]))
			return this._initElement(_element, this.elements, _index);
		else {
			Object.applyChanges(this.dict[_element[this.key]], _element);
			return this.dict[_element[this.key]].__index;
		}
	}
	/**
	 * Инициализирует элемент добавляя ему
	 * ссылку на следующий элемент и индекс
	 * вставка происходит либо по индексу либо в конец
	 * @param {any} _element - объект с ключом словаря
	 * @param {any} _arr - массив для индексации словаря
	 * @param {any} _index
	 */
	_initElement(_element, _arr, _index = null) {
		const isMiddleInsert = _index != null && _index !== this.elements.length;
		_index = isMiddleInsert ? _index : this.elements.length;
		Object.defineProperties(_element, {
			'__index': {
				value: _index,
				writable: true
			},
			'__nextElement': {
				value: _arr[isMiddleInsert ? _index : _index + 1],
				writable: true
			}
		});
		if (isMiddleInsert) {
			_arr.splice(_index, 0, _element);
			if (_index !== 0)
				this.elements[_index - 1].__nextElement = _element;
			this._increaseNextIndexes(_element);
		}
		this.dict[_element[this.key]] = _element;
		this.elements[_index] = _element;
		return _index;
	}
	/**
	 * Уменьшение индексов всех последующих элементов
	 * @param {{}} _element - элемент словаря
	 */
	_decreaseNextIndexes(_element) {
		let next = _element.__nextElement;
		while (next != null) {
			next.__index--;
			next = next.__nextElement;
		}
	}
	/**
	 * Увеличение индексов всех последующих элементов
	 * @param {{}} _element - элемент словаря
	 */
	_increaseNextIndexes(_element) {
		let next = _element.__nextElement;
		while (next != null) {
			next.__index++;
			next = next.__nextElement;
		}
	}
}