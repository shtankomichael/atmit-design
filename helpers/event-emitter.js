﻿import {
    Logger
} from 'atmit-design';

export default class EventEmitter {
    constructor() {
        this.subscribers = {};
    }

    on(nameOrObj, func) {
        let off = [];
        if (typeof (nameOrObj) == 'object') {
            for (let func in nameOrObj) {
                off.push(this._onListener(func, nameOrObj[func]));
            }
        } else {
            off.push(this._onListener(nameOrObj, func));
        }
        return () => {
            for (let func of off) {
                func();
            }
        };
    }

    off(name, func) {
        if (this.subscribers[name] == null || !this.subscribers[name].has(func))
            return false;
        return this.subscribers[name].delete(func);
    }

    _onListener(name, func) {
        if (this.subscribers[name] == null)
            this.subscribers[name] = new Map();
        this.subscribers[name].set(func, true);
        return () => {
            this.off(name, func)
        };
    }

    /**
     * вызов сокета
     * @param {string} name - function name
     * @param {?{}} data - data
     */
    emit(name, data = null) {
        const funcMap = this.subscribers[name];
        if (funcMap == null)
            return;

        for (let func of funcMap.keys()) {
            try {
                func(data);
            } catch (e) {
                Logger.log({
                    info: `event emitter _procEvent '${name}'`,
                    data,
                    message: e.message,
                    stack: e.stack
                }, 'Danger');
            }
        }
    }
}