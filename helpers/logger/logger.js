﻿class LoggerProvider {
    constructor(name) {
        this.name = name;
    }
    log() {}
}

class Logger {
    /**
     * @param {LoggerProvider} provider
     */
    static addProvider(provider) {
        Logger.providers[provider.name] = provider;
    }

    /**
     * @param {string} name
     */
    static removeProvider(name) {
        delete Logger.providers[name];
    }

    /**
     * any arguments here
     */
    static log() {
        for (let name in Logger.providers) {
            Logger.providers[name].log.apply(this, arguments);
        }
    }
}

Logger.providers = {};
export {
    Logger,
    LoggerProvider
}