﻿import { Alert } from '../../index';
import I18n from './i18n/i18n';

export default class Request {
    static get defaultOptions() {
        return {
            lang: 'ru',
            method: 'get',
            credentials: "same-origin",
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            },
            data: null,
            includeXSFRToken: true
        };
    }

    static get(options) {
        options.method = 'get';
        return Request.ajax(options);
    }
    static post(options) {
        options.method = 'post';
        return Request.ajax(options);
    }

    static ajax(options) {
        return new Promise((resolve, reject) => {
            Object.merge(options, Request.defaultOptions);
            options.i18n = I18n[options.lang];

            let url = options.url;
            let data = null;
            if (options.data != null) {
                switch (options.method.toLowerCase()) {
                    case 'get':
                        {
                            url += "?" + Object.keys(options.data)
                                .map((i) => i + '=' + encodeURIComponent(options.data[i]))
                                .join('&');
                            break;
                        }
                    case 'post':
                        {
                            if (options.data instanceof FormData) {
                                delete options.headers['Content-Type'];
                            } else {
                                data = JSON.stringify(options.data);
                            }
                            break;
                        }
                }
            }
            if (options.includeXSFRToken && window.requestVerificationToken) {
                options.headers['X-XSRF-TOKEN'] = window.requestVerificationToken;
            }
            //swap property 'data' with 'body'
            if (data != null)
                options.body = data;
            delete options.data;

            fetch(url, options)
                .then((response) => {
                    switch (response.status) {
                        case 404:
                            Alert.showToastr(options.i18n['pageNotFound'], Alert.Type.Warning);
                            return reject(response);
                        case 500:
                            Alert.showToastr(options.i18n['serverError'], Alert.Type.Danger);
                            return reject(response);
                        default:
                        case 200:
                            response.json().then(json => {
                                Request.validateResponse(json, options.i18n)
                                    ? resolve(json)
                                    : reject(json);
                            }, reject);
                            break;
                    }
                });
        });
    }

    static validateResponse(resp, i18n) {
        if (resp == null) {
            Alert.showToastr(i18n['pageNotFound'], Alert.Type.Warning);
            return false;
        }
        //TODO переписать (если result нет, то вернулся обычный json,а не наша ебанина)
        if (resp.result == null)
            return true;

        const result = resp.result;
        if (result.alerts.length !== 0 && !result.config.isManualHandle) {
            for (let alert of result.alerts)
                Alert.showToastr(alert.text, alert.type, alert.title);
        }
        //true - если с сервера пришел флаг ручной обработки ошибок
        return result.config.isManualHandle || result.isSuccess;
    }
}