﻿export default  {
    'page404': 'Page not found',
    'serverError': 'Server error'
};