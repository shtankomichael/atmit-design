﻿import KeyGenerator from './key-generator';
import Request from './request/request';

export default class ChunkedUploader {
    constructor(options = {}) {
        Object.merge(options, this.defaultOptions);
        this.options = options;
        this.options.totalChunks = Math.ceil(this.options.file.size / this.options.chunkSize);
    }

    get defaultOptions() {
        return {
            uniqueKey: KeyGenerator.getKey(),
            chunkSize: 4 * 1024 * 1024,
            currentChunk: 0,
            offset: 0,
            isUploadCancelled: false,
            currentUploadRequest: null,
            metadataUrl: null,
            uploadUrl: null,
            cancelUrl: null,
            metadata: {},
            onProgress() { },
            onError() { },
            onCancel() { }
        };
    }

    startUpload() {
        if (this.options.file == null) {
            console.log("File is null");
            return;
        }
        this.options.isUploadCancelled = false;
        if (this.options.metadataUrl != null)
            this.getMetadata().then(this.setMetadata, this.options.onError);
        else this.uploadNextChunk();
    }
    setMetadata(_metadata) {
        if (_metadata.uploadUrl != null) {
            this.options.uploadUrl = _metadata.uploadUrl;
        }
        this.options.metadata = _metadata.metadata;
        this.uploadNextChunk();
    }
    getMetadata() {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: this.options.metadataUrl,
                type: 'POST',
                success: resolve,
                error: reject
            });
        });
    }

    //логика заливки куска
    uploadNextChunk() {
        if (this.options.isUploadCancelled) {
            this.options.onProgress({ percent: 0 });
            return;
        }

        if (this.options.currentChunk >= this.options.totalChunks) {
            //this.onProgress({ percent: 100 });
            return;
        }

        this.readFileChunk().then(_data => {
            this.uploadChunkData(_data.chunk).then(_result => {
                if (this.options.isUploadCancelled) {
                    this.options.onCancel();
                    return;
                }
                var result = JSON.parse(_result);
                if (!result.success) {
                    this.options.onError();
                    return;
                }
                this.options.offset = _data.endOffset;
                this.options.currentChunk += 1;
                var percent = (this.options.offset / this.options.file.size) * 100;
                this.options.onProgress({ percent: percent }, result.success, result.data);
                this.uploadNextChunk();
            }, (e) => {
                if (this.options.isUploadCancelled)
                    this.options.onCancel();
                else this.options.onError(e);
            });
        }, this.options.onError);
    }

    readFileChunk() {
        return new Promise((resolve, reject) => {
            var begin = this.options.offset,
                end = begin + this.options.chunkSize;
            if (end > this.options.file.size)
                end = this.options.file.size;
            var reader = new FileReader();
            reader.onerror = function (evt) {
                switch (evt.target.error.code) {
                    case evt.target.error.NOT_FOUND_ERR:
                        reject('File not found');
                        break;
                    case evt.target.error.NOT_READABLE_ERR:
                        reject('File is not readable');
                        break;
                    case evt.target.error.ABORT_ERR:
                        break; // noop
                    default:
                        reject('An error occurred reading this file.');
                };
            }
            reader.onloadend = function (evt) {
                if (evt.target.readyState == FileReader.DONE) {
                    resolve({ chunk: evt.target.result, endOffset: end });
                }
            };
            var blob = this.options.file.slice(begin, end);
            reader.readAsArrayBuffer(blob);
        });
    }

    uploadChunkData(chunk) {
        return new Promise((resolve, reject) => {
            var url = this.generateUploadLink();

            let xhr = this.options.currentUploadRequest = new XMLHttpRequest();
            xhr.open("POST", url, true);
            xhr.onreadystatechange = (e) => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.responseText);
                    } else {
                        reject(xhr.statusText);
                    }
                    this.options.currentUploadRequest = null;
                }
            };
            xhr.send(chunk);
        });
    }

    generateUploadLink() {
        var urlData = {
            uniqueKey: this.options.uniqueKey,
            origFilename: this.options.file.name,
            fileSize: this.options.file.size,
            offset: this.options.offset,
            chunkSize: this.options.chunkSize,
            currentChunk: this.options.currentChunk,
            totalChunks: this.options.totalChunks,
            isLast: (this.options.currentChunk + 1) == this.options.totalChunks
        }
        Object.merge(urlData, this.options.metadata);
        return this.options.uploadUrl + '?' + $.param(urlData);
    }

    cancel() {
        this.options.isUploadCancelled = true;
        this.options.currentChunk = 0;
        this.options.offset = 0;;
        //отменяем текущий реквест
        if (this.options.currentUploadRequest != null)
            this.options.currentUploadRequest.abort();
        else this.options.onCancel();
        if (this.options.cancelUrl == null)
            return;
        $.ajax({
            url: this.options.cancelUrl,
            data: this.options.metadata,
            type: 'POST'
        });
    }

    getState() {
        return {
            filename: this.options.file.filename,
            size: this.options.file.size,
            chunks: this.options.totalChunks,
            currentChunk: this.options.currentChunk
        };
    }
};