﻿export default class Node {
    static addAutoHeight(query) {
        const elements = document.querySelectorAll(query);
        for (let node of elements) {
            Node.addNodeAutoHeight(node);
        }
    }
    static addNodeAutoHeight(node) {
        if (node != null) {
            const baseHeight = node.offsetHeight;

            node.style.overflowY = 'hidden';

            Node._autoHeight(node, baseHeight);

            node.addEventListener('input', function() {
                Node._autoHeight(node, baseHeight);
            });
        }
    }
    static _autoHeight(node, baseheight) {
        node.style.height = 'auto';
        if (node.scrollHeight > baseheight)
            node.style.height = node.scrollHeight + 'px';
    }
}