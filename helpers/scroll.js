﻿export default class Scroll {
    static toLeft(element, scrollVal, duration = 200) {
        if (scrollVal == null)
            scrollVal = 205; //200 деф. ширина + 5 деф отступ

        const newValue = element.scrollLeft - scrollVal;
        const step = Scroll._animationStep(element.scrollLeft, newValue, duration);

        if (newValue <= 100) {
            Scroll._animate(element, 'scrollLeft', 0, duration, step);
            return;
        }

        Scroll._animate(element, 'scrollLeft', newValue, duration, step);
    }

    static toRight(element, scrollVal, duration = 200) {
        if (scrollVal == null)
            scrollVal = 205; //200 деф. ширина + 5 деф отступ

        const newValue = element.scrollLeft + scrollVal;
        const step = Scroll._animationStep(element.scrollLeft, newValue, duration);

        Scroll._animate(element, 'scrollLeft', newValue, duration, step);
    }
    static _animate(node, property, newValue, duration, step) {
        if (node._scrollAnimationTimeout != null)
            clearTimeout(node._scrollAnimationTimeout);

        const oldValue = node[property];

        if (newValue > oldValue) {
            node[property] += step;
            if (node[property] === oldValue || node[property] >= newValue)
                return;
        } else {
            node[property] -= step;
            if (node[property] === oldValue || node[property] <= newValue)
                return;
        }

        node._scrollAnimationTimeout = setTimeout(() => {
            Scroll._animate(node, property, newValue, duration, step);
        }, Scroll._animationDelay);
    }
    static _animationStep(oldValue, newValue, duration) {
        let step = (oldValue - newValue) / (duration / Scroll._animationDelay);
        step = step < 0 ? -step : step;
        return step;
    }
    static get _animationDelay() {
        return 20;
    }
};