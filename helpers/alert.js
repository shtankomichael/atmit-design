﻿import toastr from 'toastr';
import 'toastr/build/toastr.min.css';

const AlertType = {
    Success: "success",
    Warning: "warning",
    Info: "info",
    Danger: "error"
};
class Alert {
    constructor() {}
    static getToastrType(alertType) {
        switch (alertType) {
            case 'success':
                return AlertType.Success;
            case 'warning':
                return AlertType.Warning;
            case 'info':
                return AlertType.Info;
            case 'danger':
            case 'error':
                return AlertType.Danger;
            default:
                console.error('toastr type not found');
        }
        return AlertType.Danger;
    }
    static showToastr(text, type, title = '', options = {}) {
        const defaultOptions = {
            timeOut: 5000,
            progressBar: true,
            closeButton: true,
            positionClass: 'toast-top-center',
            preventDuplicates: true
        };
        Object.merge(options, defaultOptions);

        toastr[Alert.getToastrType(type)](text, title, options);
    }
    static show(node, text, type, title = '') {
        debugger;
        let alertEl = document.createElement('div');
        alertEl.classList.add('alert');

        let closeEl = document.createElement('i');
        closeEl.className = "fa fa-times";
        closeEl.style.cssText = "position: absolute; right: 5px; top: 5px; cursor: pointer;";         

        let destroyAlert = () => {
            closeEl.removeEventListener('click', destroyAlert);
            alertEl.remove();
        }

        closeEl.addEventListener('click', destroyAlert)

        alertEl.appendChild(closeEl);

        switch (type) {
            case 'success':
                alertEl.classList.add('alert-success');
                break;
            case 'warning':
                alertEl.classList.add('alert-warning');
                break;
            case 'danger':
                alertEl.classList.add('alert-danger');
                break;
            case 'info':
                alertEl.classList.add('alert-info');
                break;
        }
        if(title && title.length > 0){
            let heading = document.createElement('h4');
            heading.classList.add('alert-heading');
            heading.innerText = title;
            alertEl.appendChild(heading);
        }
        let textEl = document.createElement('span');
        textEl.innerText = text;
        
        alertEl.appendChild(textEl);
        node.appendChild(alertEl);
    }
}

Alert.Type = AlertType;
export default Alert;