/**
 * @description Deep merge of two objects with the change of the left part (if the fields coincide in both objects)
 * @method applyChanges
 * @memberof Object
 * @param t {Object} result object
 * @param s {Object} object with which we merge
 * @example var obj1 = {a:1}; 
 * var obj2 = {a:2,b:3};
 * Object.applyChanges(obj1, obj2); // {a:2,b:3} 
 */
Object.applyChanges = (t, s) => {
    if (t == null) {
        t = Object.assign({}, s);
        return;
    }
    for (let k in s) {
        if (!s.hasOwnProperty(k))
            continue;
        if (t[k] != null && typeof (t[k]) === 'object' && !Array.isArray(t[k])) {
            Object.applyChanges(t[k], s[k]);
        } else
            t[k] = s[k];
    }
};

/**
 * @description Deep merge of two objects
 * @method merge
 * @memberof Object
 * @param t {Object} result object
 * @param s {Object} object with which we merge
 * @example var obj1 = {a:1}; 
 * var obj2 = {a:2,b:3};
 * Object.merge(obj1, obj2); // {a:1,b:3} 
 */
Object.merge = (t, s) => {
    if (t == null) {
        t = Object.assign({}, s);
        return;
    }
    for (let k in s) {
        if (!s.hasOwnProperty(k))
            continue;
        if (t[k] != null && typeof (t[k]) === 'object' && !Array.isArray(t[k])) {
            Object.merge(t[k], s[k]);
        } else if (typeof t[k] === 'undefined')
            t[k] = s[k];
    }
};

/**
 * @description convert object to FormData
 * @param {Object} obj 
 * @param {String} parPath
 * @returns {String}
 */
Object.toFormData = (obj, parPath) => {
    parPath = parPath == null || parPath.length === 0 ? '' : parPath;
    let resStr = '';
    if (typeof (obj) !== 'object') {
        if (parPath.length !== 0) {
            resStr += `${parPath[0] === '&' ? '' : '&'}${parPath}=${encodeURIComponent(obj)}`;
        } else {
            resStr += `&${encodeURIComponent(obj)}`;
        }
    }
    for (let k in obj) {
        if (!obj.hasOwnProperty(k))
            continue;
        const objectValue = obj[k];
        if (objectValue == null)
            continue;
        const objectType = typeof (objectValue);
        const isArray = Array.isArray(objectValue);
        const isObject = objectType === 'object';
        if (!isObject && !isArray) {
            if (parPath.length !== 0) {
                resStr += `${parPath[0] === '&' ? '' : '&'}${parPath}${encodeURIComponent('.')}${k}=${encodeURIComponent(objectValue)}`;
            } else {
                resStr += `&${k}=${encodeURIComponent(objectValue)}`;
            }
        } else if (isObject && isArray) {

            if (parPath.length !== 0) {

                for (let i = 0; i < objectValue.length; i++) {
                    const isArrayElementIsObjectOrArray = Array.isArray(objectValue) || typeof (objectValue) === 'object';
                    if (isArrayElementIsObjectOrArray) {
                        resStr += Object.toFormData(objectValue[i], `${parPath}${encodeURIComponent('.')}${k}[${i}]`);
                    } else
                        resStr += `${parPath}${encodeURIComponent('.')}${k}[${i}]=${encodeURIComponent(objectValue[i])}`;
                }
            } else {
                for (let i = 0; i < objectValue.length; i++) {
                    const isArrayElementIsObjectOrArray = Array.isArray(objectValue) || typeof (objectValue) === 'object';
                    if (isArrayElementIsObjectOrArray) {
                        resStr += Object.toFormData(objectValue[i], `&${k}[${i}]`);
                    } else
                        resStr += `&${k}[${i}]=${encodeURIComponent(objectValue[i])}`;
                }
            }
        } else if (isObject && !isArray) {
            const childParentPath = parPath.length === 0 ? k : `${parPath}.${k}`;
            resStr += `${Object.toFormData(objectValue, childParentPath)}`;
        }
    }
    if (resStr[0] === '&' && parPath.length === 0)
        resStr = resStr.substr(1, resStr.length - 1);
    return resStr;
};


/**
 * @description check object on is empty _obj=={}
 * @method isEmpty
 * @param obj {Object} result object
 * @memberof Object
 */

if (!Object.isEmpty) {
    Object.isEmpty = (obj) => {
        return Object.keys(obj).length === 0 && obj.constructor === Object;
    };
}
