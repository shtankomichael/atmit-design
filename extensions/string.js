if (!String.prototype.toSentenceCase) {
    /**
     * @description Sentence case
     * @method toSentenseCase
     * @memberof String.prototype
     * @returns {String}
     * @example var res = 'string'.toSentenseCase();
     */
    String.prototype.toSentenceCase = function () {
        var result = '';
        if (this.length === 0)
            return result;

        const inputString = `.${this}`;
        const terminalCharacters = ['.', '?', '!'];

        var terminalCharacterEncountered = false;

        for (let char of inputString) {
            if (terminalCharacterEncountered) {
                if (char === ' ')
                    result = result + char;
                else {
                    result = result + char.toUpperCase();
                    terminalCharacterEncountered = false;
                }
            } else
                result = result + char.toLowerCase();

            for (let terminalChar of terminalCharacters)
                if (char === terminalChar) {
                    terminalCharacterEncountered = true;
                    break;
                }
        }
        return result.substring(1, result.length);
    };
}

if (!String.prototype.startsWith) {
    /**
     * @description Determines if the string instance starts with another string.
     * @method startsWith
     * @memberof String.prototype
     * @param suffix {String} The string to check for.
     * @returns {boolean} True if string instance starts with the preffix string. False otherwise.
     * @example var startsWith = String.startsWith('string', 'st');
     */
    String.prototype.startsWith = function (prefix) {
        return this.indexOf(prefix) === 0;
    };
}

if (!String.prototype.isNullOrEmpty) {
    /**
     * @description check string on null or empty.
     * @method isNullOrEmpty
     * @memberof String.prototype
     * @returns {boolean} True if string is null or empty else false.
     * @example var startsWith = ""; if(startsWith.isNullOrEmpty());
     */
    String.prototype.isNullOrEmpty = function () {
        if (this === undefined || this === null || this.length === 0)
            return true;
        return false;
    };
}

if (!String.prototype.tryParseInt) {
    /**
     * @description check string on null or empty.
     * @method isNullOrEmpty
     * @memberof String.prototype
     * @returns {boolean} True if string is null or empty else false.
     * @example var startsWith = ""; if(startsWith.isNullOrEmpty());
     */
    String.prototype.tryParseInt = function (defaultValue) {
        var retValue = defaultValue;
        if (this !== undefined && this !== null && this.toString().length > 0 && !isNaN(this))
            retValue = parseInt(this);
        return retValue;
    };
}

if (!String.prototype.equals) {
    /**
     * @description check strings for a match
     * @method equals
     * @memberof String.prototype
     * @returns {boolean} True if strings are equals or false.
     */
    String.prototype.equals = function (trg, ignoreCase = false, useLocale = false) {
        let src = this;
        if (ignoreCase) {
            if (useLocale) {
                src = src.toLocaleLowerCase();
                trg = trg.toLocaleLowerCase();
            } else {
                src = src.toLowerCase();
                trg = trg.toLowerCase();
            }
        }
        return src === trg;
    };
}

if (!String.prototype.format) {
    /**
     * @description check strings for a match
     * @method equals
     * @memberof String.prototype
     * @returns {boolean} True if strings are equals or false.
     */
    String.prototype.format = function () {
        var s = this;
        // if(typeof (this != 'string'))
        //     return '';
        for (let i = 0; i < arguments.length; i++)
            s = s.replace(new RegExp(`\\{${i}\\}`, 'gm'), arguments[i]);
        return s;
    };
}

if (!String.prototype.capitalize) {
    String.prototype.capitalize = function () {
        return this.charAt(0).toUpperCase() + this.slice(1);
    };
}