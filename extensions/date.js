import Dateformat from "dateformat";

if (!Date.format) {
    /**
     * @description return date in format
     * @method format
     * @memberof Date
     * @param date {String} or {Date} or {Number}.
     * @param format {String}.
     * @returns {String} return date in format
     * @example var dateInFormat = Date.format(new Date(), 'dd.mm.yyyy');
     */
    Date.format = function (date, format = 'dd.mm.yyyy HH:MM:ss') {
        return Dateformat(date, format);
    };
}
if (!Date.getTimeFromSec) {
    /**
     * @description return time
     * @method getTimeFromSec
     * @memberof Date
     * @param sec {Number}.
     * @returns {String} return time
     * @example var time = Date.getTimeFromSec(1234);
     */
    Date.getTimeFromSec = function (sec = 0) {

        if (isNaN(sec) || sec == null)
            sec = 0;
        else
            sec = parseInt(sec);

        var hours, minutes, seconds;
        seconds = sec;

        minutes = Math.floor(seconds / 60);
        seconds = seconds % 60;

        hours = Math.floor(minutes / 60);
        minutes = minutes % 60;

        var timeStr = hours < 10 ? '0' + hours : hours;
        timeStr += ':';
        timeStr += minutes < 10 ? '0' + minutes : minutes;
        timeStr += ':';
        timeStr += seconds < 10 ? '0' + seconds : seconds;
        return timeStr;
    };
}
if (!Date.prototype.toUTC) {
    Date.prototype.toUTC = function () {
        return new Date(this.getTime() + this.getTimezoneOffset() * 60000);
    };
}
if (!Date.prototype.getMonthDays) {
    Date.prototype.getMonthDays = function () {
        
        var d = new Date(this.getFullYear(), this.getMonth() + 1, 0);
        return d.getDate();
    }
}
