if (!Number.prototype.declOfNum) {
    Number.prototype.declOfNum = function (stringArray) {
        let number = Number.parseInt(this);
        if (number < 0)
            number *= -1;
        const numArray = [2, 0, 1, 1, 1, 2];
        return stringArray[number % 100 <= 4 || number % 100 >= 20 ? numArray[number % 10 < 5 ? number % 10 : 5] : 2];
    };
}