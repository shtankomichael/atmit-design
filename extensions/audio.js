if (!HTMLAudioElement.prototype.stop) {
    // Stop Audio:
    HTMLAudioElement.prototype.stop = function () {
        this.pause();
        this.currentTime = 0.0;
    };
}

if (!HTMLAudioElement.prototype.fadeIn) {
    HTMLAudioElement.prototype.fadeIn = function (durationMs) {
        var audio = this;
        audio.volume = 0.05;
        audio.play();

        var delay = durationMs / 19; // 19 step to 1.0
        var volumeUp = function () {
            setTimeout(function () {
                try {
                    audio.volume += 0.05;
                } catch (e) {
                    audio.volume = 1;
                }
                if (audio.volume >= 1)
                    return;
                volumeUp();
            }, delay);
        }
        setTimeout(volumeUp, delay);
    };
}