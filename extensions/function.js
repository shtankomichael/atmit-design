import {
    Logger
} from '../helpers/logger/logger';
/**
 * @description safe function call
 */
if (!Function.prototype.move) {
    Function.prototype.safe = function () {
        try {
            this();
        } catch (e) {
            Logger.log(e);
        }
    };
}