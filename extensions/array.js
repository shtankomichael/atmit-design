/**
 * @description shifts an array elements
 * @param {Number} from
 * @param {Number} to
 */
if (!Array.prototype.move) {
    Array.prototype.move = function (from, to) {
        this.splice(to, 0, this.splice(from, 1)[0]);
    };
}
/**
 * @description shuffle array
 */
if (!Array.prototype.shuffle) {
    Array.prototype.shuffle = function () {
        var j, x, i;
        for (i = this.length; i; i--) {
            j = Math.floor(Math.random() * i);
            x = this[i - 1];
            this[i - 1] = this[j];
            this[j] = x;
        }
    };
}