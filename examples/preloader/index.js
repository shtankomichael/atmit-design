import { Preloader } from 'atmit-design';

let loader = Preloader.show({
    node: document.body
});
setTimeout(() => {
    loader.hide();
}, 2000);