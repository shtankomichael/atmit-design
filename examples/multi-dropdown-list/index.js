import { MultiDDL } from 'atmit-design';

new MultiDDL({
    element: "#test-ddl",
    argName: "test",
    monoMode: true,
    width: '300px',
    items: [{
        id: '1',
        text: 'qweqwe1'
    },
    {
        id: '2',
        text: 'qweqwe2'
    },
    {
        id: '3',
        text: 'qweqwe3'
    },
    {
        id: '4',
        text: 'qweqwe4'
    },
    {
        id: '5',
        text: 'qweqwe5'
    }],
    search: {
        enable: true,
        placeholder: 'search'
    },
    onSelected: function (item) {

    }
});