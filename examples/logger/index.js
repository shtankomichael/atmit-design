import {
    Logger,
    LoggerProvider
} from 'atmit-design';

class AlertProvider extends LoggerProvider {
    constructor() {
        super('alert');
    }
    log(msg, id, obj) {
        alert(`[${new Date()}] ${msg} ${id} ${JSON.stringify(obj)}`);
    }
}

class ConsoleProvider extends LoggerProvider {
    constructor() {
        super('console');
    }
    log(msg, id, obj) {
        console.log(`[${new Date()}] ${msg} ${id} ${JSON.stringify(obj)}`);
    }
}

Logger.addProvider(new AlertProvider());
Logger.addProvider(new ConsoleProvider());
Logger.log('Test log!', 123123, {a:5});