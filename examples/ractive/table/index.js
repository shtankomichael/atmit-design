import { ListRactive } from 'atmit-design';
import template from './index.hbs';

new (ListRactive.extend({
    el: '#ractive',
    template,
    data: () => ({
        url: 'http://localhost:53985/Api/Classroom/GetItems'
    }),
    on: {
        render() {
            this.updateItems();
        }
    }
}));