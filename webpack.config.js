﻿"use strict";
const path = require('path'),
	webpack = require('webpack'),
	pkg = require('./package.json'),
	copyWebpackPlugin = require('copy-webpack-plugin'),
	uglifyjsWebpackPlugin = require('uglifyjs-webpack-plugin'),
	extractTextPlugin = require('extract-text-webpack-plugin');

module.exports = (env) => {
	const isProd = env && env.prod;
	const bundleFolder = "dist/";
	const plugins = [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
			Popper: 'popper.js'
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: "vendor",
			chunks: ['atmit-design'],
			minChunks: Infinity
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: "atmit-design",
			chunks: ['/examples/modal', '/examples/ddl', '/examples/preloader', '/examples/ractive-table', '/examples/logger', '/examples/alert', '/examples/dispatcher'],
			minChunks: Infinity
		}),
		new extractTextPlugin({
			allChunks: true,
			filename: isProd ? 'css/[name].min.css' : 'css/[name].css'
		})
	];
	if (isProd) {
		plugins.push(new uglifyjsWebpackPlugin());
	}

	var excludes = [
		'ractive'
	];

	var vendorScripts = Object.keys(pkg.dependencies).filter((name) => {
		return !excludes.includes(name);
	});

	vendorScripts.push('ractive/runtime');
	return {
		entry: {
			vendor: vendorScripts,
			'atmit-design': `./index.js`,
			'/examples/modal': `@examples/modal/index.js`,
			'/examples/ddl': `@examples/multi-dropdown-list/index.js`,
			'/examples/preloader': `@examples/preloader/index.js`,
			'/examples/ractive-table': `@examples/ractive/table/index.js`,
			'/examples/logger': `@examples/logger/index.js`,
			'/examples/alert': `@examples/alert/index.js`,
			'/examples/dispatcher': `@examples/dispatcher/index.js`
		},
		stats: {
			modules: false
		},
		resolve: {
			extensions: ['.js'],
			alias: {
				'@libs': path.resolve('libs'),
				'@examples': path.resolve('examples'),
				'atmit-design': path.resolve('index.js')
			}
		},
		output: {
			filename: isProd ? '[name].min.js' : '[name].js',
			publicPath: '/dist/',
			path: path.join(__dirname, bundleFolder)
		},
		module: {
			rules: [{
					loader: "babel-loader",
					test: /\.js$/,
					exclude: /node_modules/
				},
				{
					test: /\.less$/,
					use: extractTextPlugin.extract({
						fallback: 'style-loader',
						use: [{
								loader: 'css-loader',
								options: {
									minimize: isProd
								}
							},
							{
								loader: 'less-loader'
							}
						]
					})
				},
				{
					test: /\.scss$/,
					use: extractTextPlugin.extract({
						fallback: 'style-loader',
						use: [{
								loader: 'css-loader',
								options: {
									minimize: isProd
								}
							},
							{
								loader: 'sass-loader'
							}
						]
					})

				},
				{
					test: /\.css/,
					use: extractTextPlugin.extract({
						fallback: 'style-loader',
						use: [{
							loader: 'css-loader',
							options: {
								minimize: isProd
							}
						}]
					})
				},
				{
					test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
					use: {
						loader: 'file-loader',
						options: {
							outputPath: 'fonts/'
						}
					}
				},
				{
					test: /\.(gif|jpg|png|jpe?g)([\?]?.*)$/,
					loader: "file-loader"
				},
				{
					test: /\.(html|handlebars|hbs)(\?[a-z0-9]+)?$/,
					loader: 'ractive-loader'
				}
			]
		},
		devServer: {
			contentBase: path.join(__dirname, "dist"),
			compress: true,
			port: 9000
		},
		plugins: plugins
	};
};